# app_family_status

[Документация](../readme.md) / [old](./index.md) / **app_family_status** 

| Value | Description |
| ----- | ----------- |
| 0 | FAMILY_MARRIED |
| 1 | FAMILY_NOT_MARRIED |
| 2 | FAMILY_DIVORCED |
| 3 | FAMILY_CIVIL |
