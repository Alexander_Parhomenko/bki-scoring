# Документация (old)

[Документация](../readme.md) / **old**

| name | type | example | about |
| ---- | ----- | ----- | ------ |
| order_created | timestamp | 1565136000 | дата создания заявки (created_at статуса 2) |
| user_other_credit_payes | float | 100 | выплаты по другим кредитам |
| request_credit_sum | integer | 800 | запрошенная сумма по кредиту |
| [app_industry_type_agr](./app_industry_type_agr.md) | integer | 1 | сфера работы |
| [app_employment_type](./app_employment_type.md) | integer | 0 | тип трудоустройства |
| [app_family_status](./app_family_status.md) | integer | 1 | семейное положение |
| [app_position](./app_position.md) | integer | 1 | должность |
| app_requested_period | integer | 15 | количество дней кредита |
| beh_client_reject | integer | 500 | К-во положительно принятых решений по заявкам от данного ClientID, по которым кредит не был выдан (заявка имела один из статусов UserRefused, CanceledUser) |
| app_time_last_credit | timestamp | 1564617600 | Дата создания предыдущей заявки |
| max_dpd_previous_loan | integer | 500 | Максимальное количетсво дней просрочки по последнему закрытому займу |
| previous_auto_declined | integer | 500 | Количество автоматических отказов по пердыдущим заявкам по ClientID заемщика (отказ на стадии верификации не учитывать) |
| beh_early_repaym_quant | integer | 500 | К-во досрочных погашений по пердыдущим выданным займам у данного ClientID (дата фактического полного погашения кредита раньше, чем начальная дата погашения по договору) |
| max_credit_sum_approve | integer | 2000 | Максимально сумма одобренного кредита |
| email | string | qwe@asd.zxc | email |
| phone | string | +380216545645 | phone |
| score_artellence | string | 0.15 | скор бал artellence |
| ks_scoring | string | 0.15 | скор бал kyivstar |
| vodafone_score | string | 0.15 | скор бал vodafone |
| [reg](./reg.md) | object | {"region": "123","city": "123","street": "123","build": "123","flat": "123"} | адрес регистрации |
| [live](./live.md) | object | {"region": "123","city": "123","street": "123","build": "123","flat": "123"} | адрес проживания |
