# app_employment_type

[Документация](../readme.md) / [old](./index.md) / **app_employment_type** 

| Value | Description |
| ----- | ----------- |
| 0 | работаю официально |
| 1 | работаю не официально |
| 2 | собственное дело |
| 3 | не работаю |