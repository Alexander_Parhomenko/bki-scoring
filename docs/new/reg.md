# reg

[Документация](../readme.md) / [new](./index.md) / **reg** 

| Name | Type | Example | Description |
| ---- | ---- | ------- | ----------- |
| region | string | bla bla | регион |
| city | string | bla bla |  город |
| street | string | bla bla |  улица |
| build | string | bla bla |  дом |
| flat | string | bla bla |  квартира |