# app_type_residance

[Документация](../readme.md) / [new](./index.md) / **app_type_residance** 

| Value | Description |
| ----- | ----------- |
| 0 | LIVING_TYPE_OWN_APARTMENT |
| 1 | LIVING_TYPE_RELATIVES |
| 2 | LIVING_TYPE_DORMITORY |
| 3 | LIVING_TYPE_RENT |