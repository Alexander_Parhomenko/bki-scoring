# Документация (new)

[Документация](../readme.md) / **new**

| name | type | example | about |
| ---- | ----- | ----- | ------ |
| order_created | timestamp | 1565136000 | дата создания заявки (created_at статуса 2) |
| user_register | timestamp | 1565740800 | дата регистрации пользователя (поле created_at) |
| [app_education_type](./app_education_type.md) | integer | 1 | образование |
| [app_fb_connect](./app_fb_connect.md) | integer | 1 | подключение facebook |
| user_income | integer | 5000 | зарплата из анкеты |
| user_expences | integer | 4000 | растраты |
| user_other_credit_payes| integer | 100 | выплаты по другим кредитам |
| request_credit_sum | integer | 800 | запрошенная сумма по кредиту |
| [app_industry_type_agr](./app_industry_type_agr.md) | integer | 1 | сфера работы |
| app_requested_period | integer | 15 | количество дней кредита |
| [app_type_residance](./app_type_residance.md) | integer | 1 | Тип жилья |
| email | string | qwe@asd.zxc | email |
| phone | string | +380216545645 | phone |
| email_validate| boolean | true/false | подтвержден email |
| score_artellence | string | 0.15 | скор бал artellence |
| ks_scoring | string | 0.15 | скор бал kyivstar |
| vodafone_score | string | 0.15 | скор бал vodafone |
| [reg](./reg.md) | object | {"region": "123","city": "123","street": "123","build": "123","flat": "123"} | адрес регистрации |
| [live](./live.md) | object | {"region": "123","city": "123","street": "123","build": "123","flat": "123"} | адрес проживания |