# app_education_type

[Документация](../readme.md) / [new](./index.md) / **app_education_type** 

| Value | Description |
| ----- | ----------- |
| 0 | EDUCATION_NOT_FULL_MIDDLE |
| 1 | EDUCATION_MIDDLE |
| 2 | EDUCATION_MIDDLE_SPECIAL |
| 3 | EDUCATION_NOT_FULL_HIGH |
| 4 | EDUCATION_HIGH |
| 5 | EDUCATION_TWO_HEIGHT |
| 6 | EDUCATION_SCIENCE_DEGREE |