#!/usr/bin/env sh
# composer global require "fxp/composer-asset-plugin:1.4.5"
composer install &&
./vendor/bin/phpunit &&
ln -sf `pwd`/vendor `pwd`/server/vendor &&
cd ./server/web &&
rm -rf assets &&
rm -rf tmp &&
mkdir assets &&
mkdir tmp