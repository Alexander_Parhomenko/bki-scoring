<?php

namespace ifinance\scoring\exceptions;

/**
 * Class NotFoundVersionException
 * @package ifinance\scoring\exceptions
 */
class NotFoundVersionException extends GlobalScoringExceptions
{
}
