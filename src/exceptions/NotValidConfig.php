<?php

namespace ifinance\scoring\exceptions;

/**
 * Class NotValidConfig
 * @package ifinance\scoring\exceptions
 */
class NotValidConfig extends GlobalScoringExceptions
{
}
