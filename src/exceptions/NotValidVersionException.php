<?php

namespace ifinance\scoring\exceptions;

/**
 * Class NotValidVersionException
 * @package ifinance\scoring\exceptions
 */
class NotValidVersionException extends GlobalScoringExceptions
{
}
