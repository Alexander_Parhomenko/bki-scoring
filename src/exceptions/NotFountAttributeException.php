<?php

namespace ifinance\scoring\exceptions;

/**
 * Class NotFountAttributeException
 * @package ifinance\scoring\exceptions
 */
class NotFountAttributeException extends GlobalScoringExceptions
{
}
