<?php

namespace ifinance\scoring\exceptions;

use Exception;

/**
 * Class GlobalScoringExceptions
 * @package ifinance\scoring\exceptions
 */
class GlobalScoringExceptions extends Exception
{
}
