<?php

namespace ifinance\scoring\exceptions;

/**
 * Class NotValidParamsException
 * @package ifinance\scoring\exceptions
 */
class NotValidParamsException extends GlobalScoringExceptions
{
}
