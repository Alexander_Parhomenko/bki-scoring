<?php

namespace ifinance\scoring\v1;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\exceptions\NotValidConfig;
use ifinance\scoring\exceptions\NotValidParamsException;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\ARunner;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\interfaces\Response;
use ifinance\scoring\v1\models\BaseModel;
use ifinance\scoring\v1\workers\QMfoYe;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class Runner
 * @package ifinance\scoring\v1
 */
class Runner extends ARunner
{
    /** @var AWorker[] */
    private $workers = [];

    /**
     * @var array
     */
    private $bkiFile = [];

    /**
     * @var array
     */
    private $zone = [];

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var array
     */
    private $workerResult = [];

    public function init()
    {
        Registry::on();
        BcMathFactory::instance()->bcscale(4);
    }

    /**
     * @param string $configure
     * @return Response
     * @throws NotValidConfig
     * @throws NotValidParamsException
     * @throws \Throwable
     */
    public function run(string $configure): Response
    {
        $this->workers = require __DIR__ . '/config.php';
        $config = Json::decode($configure);
        $bkiFile = ArrayHelper::getValue($config, 'bki_file', []);
        $params = ArrayHelper::getValue($config, 'params', null);
        $zone = ArrayHelper::getValue($config, 'zone', null);
        $globalData = ArrayHelper::getValue($config, 'global_data', []);

        if (!is_array($bkiFile)) {
            $bkiFile = [];
        }
        $this->validate($params, 'params');
        $this->validate($zone, 'zone');

        $this->bkiFile = $bkiFile;
        $this->params = $params;
        $this->zone = $zone;

        $this->fillParams($globalData);

        $sum = 0;
        $response = new Response();

        foreach ($params as $name => $param) {
            $score = $this->runWorker($name, 'result');
            $sum += $score;
        }

        foreach ($this->workerResult as $name => $value) {
            $response->{$name} = $value;
        }

        $response->score = $sum;
        foreach ($zone as $name => $item) {
            $checker = new Checker($item);
            if ($checker->check($sum)) {
                $response->zone = $name;
                Logger::log([
                    'zone' => $name,
                    'diapason' => $item,
                    'sum' => $sum,
                    'check' => true
                ], Logger::LOG_LEVEL_INFO);
                break;
            } else {
                Logger::log([
                    'zone' => $name,
                    'diapason' => $item,
                    'sum' => $sum,
                    'check' => false
                ], Logger::LOG_LEVEL_INFO);
            }
        }

        return $response;
    }

    private function fillParams($globalData)
    {
        try {
            if (is_array($globalData)) {
                foreach ($globalData as $key => $item) {
                    foreach ($this->params as &$value) {
                        $value['attribute'][$key] = $item;
                    }
                }
            }
        } catch (\Throwable $e) {
            Logger::log([$this->params, $globalData, $e], Logger::LOG_LEVEL_CRITICAL);
            return null;
        }
    }

    /**
     * @param string $name
     * @param string $type
     * @return float|null
     * @throws NotValidParamsException
     */
    private function runWorker(string $name, string $type): ?float
    {
        if (isset($this->workerResult[$name])) {
            return $this->workerResult[$name][$type] ?? 0;
        }

        $param = $this->params[$name] ?? [];
        $class = $this->workers[$name]['worker'] ?? null;
        if (null === $class) {
            Logger::log([
                'workers' => $this->workers,
                'not found' => $name
            ], Logger::LOG_LEVEL_CRITICAL);
            throw new NotValidParamsException($name);
        }

        $model = $this->createModel($name, $param, $this->bkiFile, $this->zone);

        $depends = $this->workers[$name]['depends'] ?? [];
        if (is_array($depends)) {
            foreach ($depends as $depend) {
                $model->setDepends($depend, $this->runWorker($depend, 'score'));
            }
        }

        /** @var AWorker $worker */
        $worker = new $class(['model' => $model]);
        if ($worker instanceof AWorker) {
            $score = $worker->run();
            $this->workerResult[$name]['result'] = $score;
            $this->workerResult[$name]['score'] = $worker->getScore();
            return $this->workerResult[$name][$type];
        }

        throw new NotValidParamsException($name);
    }

    /**
     * @param string $name
     * @param array $param
     * @param array $bkiFile
     * @param array $zone
     * @return BaseModel
     * @throws NotValidParamsException
     */
    private function createModel(string $name, array $param, array $bkiFile, array $zone): BaseModel
    {
        $config = $this->workers[$name]['modelConfig'] ?? [];
        $model = new $this->workers[$name]['model']();
        $model->json = Json::encode([$name => $param]);
        $model->bkiFile = $bkiFile;
        $model->zone = $zone;

        foreach ($config as $item => $value) {
            $model->{$item} = $value;
        }

        if (!($model instanceof BaseModel)) {
            Logger::log($model->errors, Logger::LOG_LEVEL_CRITICAL);
            throw new NotValidParamsException($name);
        }

        if (!$model->validate()) {
            Logger::log($model->errors, Logger::LOG_LEVEL_CRITICAL);
            throw new NotValidParamsException(Json::encode($model->errors));
        }

        return $model;
    }

    /**
     * @param $data
     * @param $fieldName
     * @throws NotValidConfig
     */
    public function validate($data, $fieldName): void
    {
        $error = [];
        if (null === $data) {
            $error[] = 'not exists ' . $fieldName;
        }

        if (!is_array($data) || !count($data)) {
            $error[] = 'params ' . $fieldName . ' must be array';
        }

        if (count($error)) {
            throw new NotValidConfig(implode(', ', $error));
        }
    }
}
