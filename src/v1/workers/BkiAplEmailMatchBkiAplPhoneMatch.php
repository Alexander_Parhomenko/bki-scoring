<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\NormalizePhone;

/**
 * Class BkiAplEmailMatchBkiAplPhoneMatch
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BkiAplEmailMatchBkiAplPhoneMatch $model
 */
class BkiAplEmailMatchBkiAplPhoneMatch extends AWorker
{
    const EMAIL_AND_NUMBER_MATCH = 1;
    const EMAIL_NOT_MATCH_PHONE_MATCH = 2;
    const EMAIL_MATCH_PHONE_NOT_MATCH = 3;
    const EMAIL_AND_PHONE_NOT_MATCH = 4;
    const OTHER = 5;

    use NormalizeData;
    use GetAttribute;
    use NormalizePhone;

    /**
     *
     */
    public function initDefaultScore(): void
    {
        $this->score = self::OTHER;
    }


    /**
     *
     */
    protected function initScore(): void
    {
        $contactHistory = $this->model->bkiFile['contactHistory'] ?? [];
        $cont = $contactHistory['cont'] ?? [];

        if (!count($cont)) {
            return;
        }

        $cont = $this->normalizeData($cont);

        $emails = [];
        $phones = [];

        $email = strtolower($this->model->email);
        $phone = $this->normalizePhoneNumber($this->model->phone);

        foreach ($cont as $item) {
            $cType = $this->getAttribute('ctype', $item, null);
            if (in_array((int)$cType, $this->model->emailCType, true)) {
                $emails[] = $this->getAttribute('cval', $item, null, true);
            }
            if (in_array((int)$cType, $this->model->phoneCType, true)) {
                $phones[] = $this->getAttribute('cval', $item, null, true);
            }
        }

        $this->normalizePhoneNumbers($phones);
        $this->setScore($phone, $phones, $email, $emails);
    }

    /**
     * @param string $phone
     * @param string[] $phones
     * @param string $email
     * @param string[] $emails
     */
    public function setScore(string $phone, array $phones, string $email, array $emails)
    {
        $phones = array_unique($phones);
        $emails = array_unique($emails);
        Logger::log([$phone, $phones, $email, $emails], Logger::LOG_LEVEL_ALL);

        switch (true) {
            case in_array($phone, $phones, true) && in_array($email, $emails, true):
                $this->score = self::EMAIL_AND_NUMBER_MATCH;
                return;
                break;
            case count($emails) && !in_array($email, $emails, true) && in_array($phone, $phones, true):
                $this->score = self::EMAIL_NOT_MATCH_PHONE_MATCH;
                return;
                break;
            case !in_array($phone, $phones, true) && in_array($email, $emails, true):
                $this->score = self::EMAIL_MATCH_PHONE_NOT_MATCH;
                return;
                break;
            case count($emails) && count($phones) &&
                !in_array($phone, $phones, true) && !in_array($email, $emails, true):
                $this->score = self::EMAIL_AND_PHONE_NOT_MATCH;
                return;
                break;
        }

        $this->score = self::OTHER;
    }
}
