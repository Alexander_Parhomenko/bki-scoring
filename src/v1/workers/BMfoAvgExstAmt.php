<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\Checker;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\ArraySumFloat;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetFirstCreditItem;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;
use yii\helpers\Json;

/**
 * Class BMfoAvgExstAmt
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BMfoAvgExstAmt $model
 */
class BMfoAvgExstAmt extends AWorker
{
    use GetAttribute;
    use GetCrDeal;
    use NormalizeData;
    use ValidateDlamt;
    use ValidateCreditDlflStat;
    use GetFirstCreditItem;
    use GetLastCreditItem;
    use GetDealLife;
    use ArraySumFloat;
    use ArrayStringToLower;

    /**
     *
     */
    protected function initScore(): void
    {
        try {
            $report = $this->model->bkiFile;
            $crDeal = $this->getCrDeal($report);

            if (null === $crDeal) {
                return;
            }

            $crDeal = $this->normalizeData($crDeal);

            $avg = [];

            foreach ($crDeal as $credit) {
                $dldonor = $this->getAttribute('dldonor', $credit, '', true);
                $validateDldonor = in_array($dldonor, $this->arrayStringToLower($this->model->dldonor), true);
                $validateStat = $this->validateStat($credit, $this->model->dlflstat);
                Logger::log([
                    '$dldonor' => $dldonor,
                    '$validateDldonor' => $validateDldonor,
                    '$validateStat' => $validateStat
                ], Logger::LOG_LEVEL_INFO);
                if ($validateDldonor && $validateStat) {
                    $dlamt = $this->getAttribute('dlamt', $credit);
                    if ($this->validateDlamt($dlamt)) {
                        Logger::log(['dlamt' => $dlamt], Logger::LOG_LEVEL_ALL);
                        $avg[] = $dlamt;
                    } else {
                        $firstItem = $this->getFirstCreditItem($credit);
                        $dlamtcur = $this->getAttribute('dlamtcur', $firstItem);
                        if (is_numeric($dlamtcur)) {
                            Logger::log(['dlamtcur' => $dlamtcur], Logger::LOG_LEVEL_ALL);
                            $avg[] = $dlamtcur;
                        }
                    }
                }
            }
            $arrSum = $this->arraySumFloat($avg);

            Logger::log([
                '$arrSum' => $arrSum,
                '$avg' => $avg
            ], Logger::LOG_LEVEL_ALL);

            if (count($avg)) {
                $this->score = BcMathFactory::instance()->bcdiv($arrSum, count($avg));
            }
        } catch (\Throwable $e) {
            Logger::logError($e, Logger::LOG_LEVEL_CRITICAL);
            $this->score = null;
        }
    }
}
