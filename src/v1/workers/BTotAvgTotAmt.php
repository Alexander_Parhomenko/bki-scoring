<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\v1\workers\traits\ArraySumFloat;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetFirstCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;

/**
 * Class BTotAvgTotAmt
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BTotAvgTotAmt $model
 */
class BTotAvgTotAmt extends AWorker
{
    use GetCrDeal;
    use GetAttribute;
    use NormalizeData;
    use GetFirstCreditItem;
    use GetDealLife;
    use ValidateDlamt;
    use ArraySumFloat;

    /**
     * @var array
     */
    private $value = [];

    /**
     *
     */
    protected function initScore(): void
    {
        $report = $this->model->bkiFile;
        $crDeal = $this->getCrDeal($report);

        if (null === $crDeal) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);
        foreach ($crDeal as $credit) {
            $dlamt = $this->getAttribute('dlamt', $credit);
            if ($this->validateDlamt($dlamt)) {
                $this->value[] = $dlamt;
            } else {
                $firstItem = $this->getFirstCreditItem($credit);
                $dlamtcur = $this->getAttribute('dlamtcur', $firstItem);
                if ($this->validateDlamt($dlamtcur) || floatval($dlamtcur) === 0.0) {
                    $this->value[] = $dlamtcur;
                }
            }
        }

        if (count($this->value)) {
            $this->score = BcMathFactory::instance()->bcdiv(
                $this->arraySumFloat($this->value),
                count($this->value)
            );
        }
    }
}
