<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;

/**
 * Class CityRes
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\CityRes $model
 */
class CityRes extends AWorker
{
    use ArrayStringToLower;

    /**
     *
     */
    protected function initScore(): void
    {
        foreach ($this->model->group as $group) {
            if (in_array(strtolower($this->model->city), $this->arrayStringToLower($group['values']), true)) {
                $this->score = $group['name'];
                break;
            }
        }
    }
}
