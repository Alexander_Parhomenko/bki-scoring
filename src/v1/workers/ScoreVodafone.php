<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;

/**
 * Class ScoreVodafone
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\ScoreVodafone $model
 */
class ScoreVodafone extends AWorker
{
    use ValidateDlamt;

    /**
     *
     */
    protected function initScore(): void
    {
        if ($this->validateDlamt($this->model->score)) {
            $this->score = (float) $this->model->score;
        }
    }
}
