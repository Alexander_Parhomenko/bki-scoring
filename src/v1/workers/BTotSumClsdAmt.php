<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetFirstCreditItem;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;

/**
 * Class BTotSumClsdAmt
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BTotSumClsdAmt $model
 */
class BTotSumClsdAmt extends AWorker
{
    use GetCrDeal;
    use ArrayStringToLower;
    use GetAttribute;
    use ValidateDlamt;
    use GetFirstCreditItem;
    use GetLastCreditItem;
    use GetDealLife;
    use NormalizeData;
    use ValidateCreditDlflStat;

    /**
     *
     */
    protected function initScore(): void
    {
        $crDeal = $this->getCrDeal($this->model->bkiFile);

        if (null === $crDeal) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);

        foreach ($crDeal as $creditId => $credit) {
            $validateStat = $this->validateStat($credit, $this->model->dlFlStat);
            if ($validateStat) {
                $sum = null;
                $dlamt = $this->getAttribute('dlamt', $credit);
                if ($this->validateDlamt($dlamt)) {
                    Logger::log(['id' => $creditId, '$dlamt' => $dlamt], Logger::LOG_LEVEL_INFO);
                    $sum = $dlamt;
                } else {
                    $first = $this->getFirstCreditItem($credit);
                    $dlamtcur = $this->getAttribute('dlamtcur', $first);
                    if ($this->validateDlamt($dlamtcur)) {
                        Logger::log(['id' => $creditId, '$dlamtcur' => $dlamtcur], Logger::LOG_LEVEL_INFO);
                        $sum = $dlamtcur;
                    } else {
                        Logger::log(['id' => $creditId, 'ignore' => $dlamtcur], Logger::LOG_LEVEL_ALL);
                    }
                }
                if (null !== $sum) {
                    if (null === $this->score) {
                        $this->score = $sum;
                    } else {
                        $this->score = BcMathFactory::instance()->bcadd($this->score, $sum);
                    }
                }
            }
        }
    }
}
