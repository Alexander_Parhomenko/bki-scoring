<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;

/**
 * Class BTotMaxOvrdClsdLns
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BTotMaxOvrdClsdLns $model
 */
class BTotMaxOvrdClsdLns extends AWorker
{
    use GetAttribute;
    use GetLastCreditItem;
    use NormalizeData;
    use ArrayStringToLower;
    use GetDealLife;
    use GetCrDeal;
    use ValidateCreditDlflStat;
    use ValidateDlamt;

    /**
     *
     */
    protected function initScore(): void
    {
        $crDeal = $this->getCrDeal($this->model->bkiFile);

        if (null === $crDeal) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);

        foreach ($crDeal as $credit) {
            $validateStat = $this->validateStat($credit, $this->model->dlFlStat);
            if ($validateStat) {
                $last = $this->getLastCreditItem($credit);
                $dlamtExps = [];
                $dealLife = $this->getAttribute('deallife', $credit);
                foreach ($dealLife as $creditItem) {
                    $dlamtExps[] = $this->getAttribute('dlamtexp', $creditItem);
                }
                $dlamtExp = max($dlamtExps);

                if (null === $this->score) {
                    $this->score = $dlamtExp;
                } else {
                    try {
                        $this->score = (BcMathFactory::instance()->bccomp($this->score, $dlamtExp) < 0)
                            ? $dlamtExp
                            : $this->score;
                    } catch (\Throwable $e) {
                        Logger::logError($e, Logger::LOG_LEVEL_INFO);
                    }
                }
            }

            Logger::log([
                '$validateStat' => $validateStat,
                '$dlamtExp' => $dlamtExp ?? null,
            ], Logger::LOG_LEVEL_ALL);
        }
    }
}
