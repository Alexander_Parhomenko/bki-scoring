<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;

/**
 * Class BMfoSumClsdAmt
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BMfoSumClsdAmt $model
 */
class BMfoSumClsdAmt extends AWorker
{
    use GetCrDeal;
    use NormalizeData;
    use GetAttribute;
    use ValidateCreditDlflStat;
    use GetLastCreditItem;
    use GetDealLife;
    use ValidateDlamt;
    use ArrayStringToLower;

    /**
     * @var string|null
     */
    private $oldValue;

    /**
     *
     */
    protected function initScore(): void
    {
        $report = $this->model->bkiFile;
        $crDeal = $this->getCrDeal($report);

        if (null === $crDeal) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);

        if (!count($crDeal)) {
            return;
        }

        foreach ($crDeal as $credit) {
            $dlamt = $this->getDlamt($credit);
            Logger::log(['dlamt' => $dlamt], Logger::LOG_LEVEL_INFO);
            if (null !== $dlamt) {
                try {
                    $this->oldValue = $this->score;
                    $this->score = BcMathFactory::instance()->bcadd($this->score, $dlamt);
                } catch (\Exception $e) {
                    Logger::log($e, Logger::LOG_LEVEL_INFO);
                    $this->score = $this->oldValue;
                }
            }
        }
    }

    /**
     * @param array $credit
     * @return string|null
     */
    private function getDlamt(array $credit): ?string
    {
        if ($this->validateDonor($credit) && $this->validateStat($credit, $this->model->dlflstat)) {
            $value = $this->getAttribute('dlamt', $credit);
            Logger::log(['dlamt' => $value], Logger::LOG_LEVEL_INFO);
            if (null === $this->score) {
                $this->score = 0;
            }
            if (!$this->validateDlamt($value)) {
                $dlamYear = null;
                $dlamMonth = null;
                $crdeal = $this->getAttribute('deallife', $credit);

                if (null === $crdeal || !is_array($crdeal) || !count($crdeal)) {
                    return null;
                }

                $crdeal = $this->normalizeData($crdeal);
                foreach ($crdeal as $key => $item) {
                    $year = $this->getAttribute('dlyear', $item);
                    $month = $this->getAttribute('dlmonth', $item);
                    if ((null === $dlamYear && null !== $year) ||
                        ($year <= $dlamYear && $month <= $dlamMonth)
                    ) {
                        $dlamYear = $year;
                        $dlamMonth = $month;
                        $value = $this->getAttribute('dlamtcur', $item);
                    }
                }
            }

            return $value;
        }
        return null;
    }

    /**
     * @param array $credit
     * @return bool
     */
    private function validateDonor(array $credit): bool
    {
        $donor = $this->getAttribute('dldonor', $credit, null, true);
        Logger::log(
            [
                '$donor' => $donor,
                'validate' => $this->model->dldonor
            ],
            Logger::LOG_LEVEL_INFO
        );
        return in_array($donor, $this->model->dldonor, true);
    }
}
