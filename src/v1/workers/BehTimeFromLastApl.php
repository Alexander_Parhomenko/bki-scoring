<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\v1\workers\traits\ConvertDate;

/**
 * Class BehTimeFromLastApl
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BehTimeFromLastApl $model
 */
class BehTimeFromLastApl extends AWorker
{
    use ConvertDate;

    /**
     *
     */
    protected function initScore(): void
    {
        $orderCreated = $this->convertSecondToDay($this->model->orderCreated);
        $timeLastCredit = $this->convertSecondToDay($this->model->timeLastCredit);
        $sub = $orderCreated - $timeLastCredit;
        $this->score = $sub;
    }
}
