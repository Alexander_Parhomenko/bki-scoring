<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\v1\workers\traits\ConvertDate;

/**
 * Class DaysRegistered
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\DaysRegistered $model
 */
class DaysRegistered extends AWorker
{
    use ConvertDate;

    /**
     *
     */
    protected function initScore(): void
    {
        $created = $this->roundingDateToDayByTimezone($this->model->created, 'Europe/Kiev');
        $register = $this->roundingDateToDayByTimezone($this->model->register, 'Europe/Kiev');
        $sub = $created - $register;
        $day = 24 * 60 * 60;
        $this->score = (int) BcMathFactory::instance()->bcdiv($sub, $day);
    }
}
