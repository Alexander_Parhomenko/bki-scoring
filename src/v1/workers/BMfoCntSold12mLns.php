<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\ConvertDate;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;

/**
 * Class BMfoCntSold12mLns
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BMfoCntSold12mLns $model
 */
class BMfoCntSold12mLns extends AWorker
{
    use GetCrDeal;
    use GetAttribute;
    use NormalizeData;
    use ValidateCreditDlflStat;
    use GetLastCreditItem;
    use GetDealLife;
    use ArrayStringToLower;
    use ConvertDate;

    /**
     *
     */
    protected function initScore(): void
    {
        $crDeal = $this->getCrDeal($this->model->bkiFile);

        if (null === $crDeal) {
            return;
        }

        $this->score = 0;
        $crDeal = $this->normalizeData($crDeal);

        foreach ($crDeal as $creditId => $credit) {
            $validateStat = $this->validateStat($credit, $this->model->dlflstat);
            $dlDonor = $this->getAttribute('dldonor', $credit, null, true);
            $lastItem = $this->getLastCreditItem($credit);
            $dlDateClc = $this->getAttribute('dldateclc', $lastItem);
            Logger::log([
                '$validateStat' => $validateStat,
                '$dlDonor' => $dlDonor,
                '$dlDateClc' => $dlDateClc
            ], Logger::LOG_LEVEL_ALL);
            try {
                $validateDlDonor = in_array($dlDonor, $this->arrayStringToLower($this->model->dldonor), true);
                $validateDlDateClc = (
                        $this->roundingDateToDay($this->model->created) - strtotime($dlDateClc)
                    ) < $this->convertDayToSecond($this->model->maxDldateclc);
                Logger::log([
                    '$validateDlDonor' => $validateDlDonor,
                    '$validateDlDateClc' => $validateDlDateClc
                ]);

                if ($validateStat && $validateDlDateClc && $validateDlDonor) {
                    $this->score++;
                    Logger::log([
                        'credit_id' => $creditId,
                        'total_score' => $this->score
                    ]);
                }
            } catch (\Throwable $e) {
                Logger::log([
                    'message' => $e->getMessage(),
                    'status' => 'error'
                ], Logger::LOG_LEVEL_INFO);
            }
        }
    }
}
