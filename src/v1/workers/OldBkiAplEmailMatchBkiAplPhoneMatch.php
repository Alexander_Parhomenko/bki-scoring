<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\NormalizePhone;

/**
 * Class OldBkiAplEmailMatchBkiAplPhoneMatch
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\OldBkiAplEmailMatchBkiAplPhoneMatch $model
 */
class OldBkiAplEmailMatchBkiAplPhoneMatch extends BkiAplEmailMatchBkiAplPhoneMatch
{
    const EMAIL_AND_NUMBER_MATCH = 3;
    const GROUP_1 = 1;
    const GROUP_2 = 2;

    /**
     *
     */
    public function initDefaultScore(): void
    {
        $this->score = self::GROUP_2;
    }


    /**
     * @param string $phone
     * @param string[] $phones
     * @param string $email
     * @param string[] $emails
     */
    public function setScore(string $phone, array $phones, string $email, array $emails)
    {
        switch (true) {
            case in_array($phone, $phones, true) && in_array($email, $emails, true):
                $this->score = self::EMAIL_AND_NUMBER_MATCH;
                break;
            case count($emails) && count($phones) &&
                !in_array($email, $emails, true) && in_array($phone, $phones, true):
            case count($emails) && count($phones) &&
                in_array($email, $emails, true) && !in_array($phone, $phones, true):
                $this->score = self::GROUP_1;
                break;
            default:
                $this->score = self::GROUP_2;
                break;
        }
    }
}
