<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;

/**
 * Class BTotMaxDpdTotLns100
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BTotMaxDpdTotLns100 $model
 */
class BTotMaxDpdTotLns100 extends AWorker
{
    use GetCrDeal;
    use NormalizeData;
    use GetDealLife;
    use GetAttribute;
    use ValidateDlamt;

    /**
     *
     */
    protected function initScore(): void
    {
        $crDeal = $this->getCrDeal($this->model->bkiFile);

        $crDeal = $this->normalizeData($crDeal);
        if (null === $crDeal) {
            return;
        }

        foreach ($crDeal as $credit) {
            $dealLife = $this->getDealLife($credit);
            $dealLife = $this->normalizeData($dealLife);
            foreach ($dealLife as $creditItem) {
                $dlamtexp = $this->getAttribute('dlamtexp', $creditItem);
                $dldayexp = $this->getAttribute('dldayexp', $creditItem);
                Logger::log([
                    '$dlamtexp' => $dlamtexp,
                    '$dldayexp' => $dldayexp,
                ], Logger::LOG_LEVEL_ALL);
                if ($this->validateDlamt($dlamtexp) &&
                    BcMathFactory::instance()->bccomp($dlamtexp, 100) > 0) {
                    if (null === $this->score) {
                        $this->score = $dldayexp;
                    } else {
                        try {
                            if (BcMathFactory::instance()->bccomp($this->score, $dldayexp) < 0) {
                                $this->score = $dldayexp;
                            }
                        } catch (\Throwable $e) {
                            Logger::log($e, Logger::LOG_LEVEL_CRITICAL);
                        }
                    }
                }
            }
        }
        if (null !== $this->score) {
            $this->score = (float) $this->score;
        }
    }
}
