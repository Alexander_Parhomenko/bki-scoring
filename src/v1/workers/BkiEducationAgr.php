<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use yii\helpers\ArrayHelper;
use yii2mod\query\ArrayQuery;

/**
 * Class BkiEducationAgr
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BkiEducationAgr $model
 */
class BkiEducationAgr extends AWorker
{
    use NormalizeData;

    /**
     *
     */
    protected function initScore(): void
    {
        $ident = ArrayHelper::getValue($this->model->bkiFile, 'identity.cki.ident');
        $ident = $this->normalizeData($ident);

        if (!is_array($ident) || !count($ident)) {
            return;
        }

        $query = new ArrayQuery();
        $query->from($this->getIdent($ident));

        $query->orderBy(['id' => SORT_ASC, 'vdate' => SORT_DESC])->limit(1);
        $last = $query->one();
        $ceducref = mb_convert_encoding($last['ceducref'] ?? '-1', 'UTF-8');
        Logger::log(['$ceducref' => $ceducref], Logger::LOG_LEVEL_ALL);

        switch ($ceducref) {
            case 'высшее':
            case 'научная степень':
                $this->score = 1;
                break;
            case 'неоконченное высшее':
                $this->score = 2;
                break;
            case 'среднее техническое':
            case 'среднее':
            case 'неоконченное среднее':
            case 'самообразование':
                $this->score = 3;
                break;
            default:
                Logger::log(['$ceducref' => $ceducref], Logger::LOG_LEVEL_INFO);
                $this->score = 4;
                break;
        }
    }

    /**
     * @param array $ident
     * @return array
     */
    private function getIdent(array $ident): array
    {
        $res = [];
        foreach ($ident as $id => $item) {
            $tmp = $item['@attributes'];
            $tmp['id'] = $id;
            $res[] = $tmp;
        }
        return $res;
    }
}
