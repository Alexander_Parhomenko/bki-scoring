<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\ConvertDate;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;
use ifinance\scoring\v1\workers\traits\ValidateDlamtexp;
use ifinance\scoring\v1\workers\traits\ValidateDlamtexps;
use ifinance\scoring\v1\workers\traits\ValidateDldateclc;

/**
 * Class BTotMaxcDpdExstLns100And60
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BTotMaxcDpdExstLns100And60 $model
 */
class BTotMaxcDpdExstLns100And60 extends AWorker
{
    use GetCrDeal;
    use NormalizeData;
    use ValidateCreditDlflStat;
    use GetLastCreditItem;
    use GetAttribute;
    use GetDealLife;
    use ValidateDlamtexps;
    use ValidateDlamtexp;
    use ValidateDldateclc;
    use ArrayStringToLower;
    use ConvertDate;

    /**
     * @var
     */
    private $oldValue;

    /**
     *
     */
    protected function initScore(): void
    {
        $report = $this->model->bkiFile;
        $crDeal = $this->getCrDeal($report);

        if (null === $crDeal) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);

        if (!count($crDeal)) {
            return;
        }

        foreach ($crDeal as $creditId => $credit) {
            $dlDonor = $this->getAttribute('dldonor', $credit);
            Logger::log([
                'credit_id' => $creditId,
                'validateStat' => $this->validateStat($credit, $this->model->dlflstat),
                'validateDlamtexps' => $this->validateDlamtexps($credit, $this->model->dlamtexp),
                'dldonor' => [
                    'expected' => $this->model->dldonor,
                    'actual' => strtolower($dlDonor)
                ],
                'validateDldateclc' => $this->validateDldateclc(
                    $credit,
                    $this->roundingDateToDay($this->model->created),
                    $this->convertDayToSecond($this->model->dldateclc),
                    false
                ),
            ], Logger::LOG_LEVEL_ALL);
            if ($this->validateStat($credit, $this->model->dlflstat) &&
                $this->validateDlamtexps($credit, $this->model->dlamtexp) &&
                in_array(strtolower($dlDonor), $this->arrayStringToLower($this->model->dldonor), true) &&
                $this->validateDldateclc(
                    $credit,
                    $this->roundingDateToDay($this->model->created),
                    $this->convertDayToSecond($this->model->dldateclc),
                    false
                )
            ) {
                $creditItem = $this->getLastCreditItem($credit);
                $dldayexp = $this->getAttribute('dldayexp', $creditItem);
                Logger::log([
                    'dldayexp' => $dldayexp,
                ], Logger::LOG_LEVEL_INFO);
                try {
                    $this->oldValue = $this->score;
                    if (null === $this->score) {
                        $this->score = 0;
                    }
                    $isHigh = BcMathFactory::instance()->bccomp($dldayexp, $this->score);
                    if ($isHigh > 0) {
                        $this->score = $dldayexp;
                        Logger::log([
                            'new score' => $this->score
                        ], Logger::LOG_LEVEL_INFO);
                    }
                } catch (\Throwable $e) {
                    Logger::log([
                        'value' => $this->score,
                        'old_value' => $this->oldValue,
                        'error' => $e->getMessage()
                    ], Logger::LOG_LEVEL_CRITICAL);
                    $this->score = $this->oldValue;
                }
            }
        }
    }
}
