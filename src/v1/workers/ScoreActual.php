<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use yii\helpers\ArrayHelper;

/**
 * Class ScoreActual
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\ScoreActual $model
 */
class ScoreActual extends AWorker
{
    /**
     *
     */
    protected function initScore(): void
    {
        $score = ArrayHelper::getValue($this->model->bkiFile, 'creditRating.urating.@attributes.score', 0);
        $this->score = floatval($score);
    }
}
