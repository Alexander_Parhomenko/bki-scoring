<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\v1\workers\traits\NormalizePhone;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;

/**
 * Class ScoreKyivstarNonKSNumbers
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\ScoreKyivstarNonKSNumbers $model
 */
class ScoreKyivstarNonKSNumbers extends AWorker
{
    use ValidateDlamt;
    use NormalizePhone;
    const NA = -1;

    /**
     *
     */
    protected function initScore(): void
    {
        $phone = $this->normalizePhoneNumber($this->model->phoneNumber);
        $phoneCode = str_split($phone, 3);
        $ksCode = [
            '067',
            '068',
            '096',
            '097',
            '098'
        ];
        if (in_array($phoneCode[0], $ksCode, true)) {
            return;
        }

        if ($this->model->score === 'NA') {
            $this->score = self::NA;
            return;
        }

        if ($this->validateDlamt($this->model->score)) {
            $this->score = $this->model->score;
        }
    }
}
