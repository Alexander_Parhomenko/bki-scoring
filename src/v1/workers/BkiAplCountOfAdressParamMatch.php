<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use yii\helpers\ArrayHelper;

/**
 * Class BkiAplCountOfAdressParamMatch
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BkiAplCountOfAdressParamMatch $model
 */
class BkiAplCountOfAdressParamMatch extends AWorker
{
    use NormalizeData;
    use GetAttribute;

    /**
     *
     */
    protected function initScore(): void
    {
        $this->score = 0;
        $addr = ArrayHelper::getValue($this->model->bkiFile, 'identity.cki.addr');

        if (!is_array($addr)) {
            return;
        }

        $addr = $this->normalizeData($addr);

        $adState = [];
        $adCity = [];
        $adStreet = [];
        $adHome = [];
        $adFlat = [];

        foreach ($addr as $address) {
            $adState[] = $this->getAttribute('adstate', $address, '');
            $adCity[] = $this->getAttribute('adcity', $address, '');
            $adStreet[] = $this->getAttribute('adstreet', $address, '');
            $adHome[] = $this->getAttribute('adhome', $address, '');
            $adFlat[] = $this->getAttribute('adflat', $address, '');
        }

        Logger::log([
            '$adState' => $adState,
            '$adCity' => $adCity,
            '$adStreet' => $adStreet,
            '$adHome' => $adHome,
            '$adFlat' => $adFlat
        ], Logger::LOG_LEVEL_ALL);

        if ($this->inArray($this->model->regRegion, $adState, 4) ||
            $this->inArray($this->model->liveRegion, $adState, 4)) {
            $this->score++;
            Logger::log(['region' => 'success'], Logger::LOG_LEVEL_ALL);
        }

        if ($this->inArray($this->model->regCity, $adCity, 4) ||
            $this->inArray($this->model->liveCity, $adCity, 4)) {
            $this->score++;
            Logger::log(['city' => 'success'], Logger::LOG_LEVEL_ALL);
        }

        if ($this->inArray($this->model->regStreet, $adStreet, 4) ||
            $this->inArray($this->model->liveStreet, $adStreet, 4)) {
            $this->score++;
            Logger::log(['street' => 'success'], Logger::LOG_LEVEL_ALL);
        }

        if ($this->inArray($this->model->regBuild, $adHome, 4) ||
            $this->inArray($this->model->liveBuild, $adHome, 4)) {
            $this->score++;
            Logger::log(['build' => 'success'], Logger::LOG_LEVEL_ALL);
        }

        if ($this->inArray($this->model->regFlat, $adFlat, 4) ||
            $this->inArray($this->model->liveFlat, $adFlat, 4)) {
            $this->score++;
            Logger::log(['flat' => 'success'], Logger::LOG_LEVEL_ALL);
        }
    }

    private function inArray(?string $text, array $data, int $countChar): bool
    {
        if (null === $text) {
            return false;
        }
        $data = array_unique($data);
        $text = mb_strtolower(trim($text));

        if ($text === 'null') {
            return false;
        }

        $textArr = mb_str_split($text);
        foreach ($data as $item) {
            if (is_string($item)) {
                $item = mb_strtolower(trim($item));

                Logger::log(['check' => ['text' => $text, 'item' => $item]], Logger::LOG_LEVEL_ALL);
                if ($item === 'null') {
                    continue;
                }
                $itemArr = mb_str_split($item);
                if (count($itemArr)) {
                    for ($i = 0; $i < $countChar; $i++) {
                        if (($textArr[$i] ?? null) !== ($itemArr[$i] ?? null)) {
                            continue 2;
                        }
                    }
//                    Logger::log(['text' => $text, 'item' => $item], Logger::LOG_LEVEL_ALL);
                    return true;
                }
            }
        }
        return false;
    }
}
