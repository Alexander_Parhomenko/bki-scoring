<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\ConvertDate;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\NormalizeData;

/**
 * Class QMfo
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\QMfo $model
 */
abstract class QMfo extends AWorker
{
    use NormalizeData;
    use GetAttribute;
    use ConvertDate;
    use ArrayStringToLower;

    /**
     *
     */
    protected function initScore(): void
    {
        $report = $this->model->bkiFile;
        $credRes = $report['loanRequests']['credres'] ?? null;

        if (null === $credRes) {
            return;
        }

        $credRes = $this->normalizeData($credRes);

        foreach ($credRes as $request) {
            $org = strtolower($this->getAttribute('org', $request));
            $reqReason = strtolower($this->getAttribute('reqreason', $request));
            $reDate = $this->getAttribute('redate', $request);
            $validateOrg = in_array($org, $this->arrayStringToLower($this->model->org), true);
            $validateReqReason = in_array($reqReason, $this->arrayStringToLower($this->model->reqReason), true);

            Logger::log([
                '$org' => $org,
                '$reqReason' => $reqReason,
                '$reDate' => $reDate,
                '$validateOrg' => $validateOrg,
                '$validateReqReason' => $validateReqReason
            ], Logger::LOG_LEVEL_INFO);

            if ($validateOrg && $validateReqReason) {
                if (null === $this->score) {
                    $this->score = 0;
                }
                try {
                    $reDate = (new \DateTime($reDate, new \DateTimeZone('Europe/Kiev')))->getTimestamp();
                    $sub = BcMathFactory::instance()->bcsub(
                        $this->roundingDateToDayByTimezone($this->model->orderCreated,'Europe/Kiev'),
                        $this->roundingDateToDayByTimezone($reDate,'Europe/Kiev')
                    );
                    $validateReDate = BcMathFactory::instance()->bccomp(
                        $sub,
                        $this->convertDayToSecond($this->model->maxReDate)
                    ) <= 0;
                    Logger::log(['$validateReDate' => $validateReDate], Logger::LOG_LEVEL_INFO);

                    if ($validateReDate) {
                        $this->score++;
                    }
                } catch (\Throwable $e) {
                    Logger::logError($e, Logger::LOG_LEVEL_INFO);
                }
            }
        }
    }
}
