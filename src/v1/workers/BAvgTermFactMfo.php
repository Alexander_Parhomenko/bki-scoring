<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\ConvertDate;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;

/**
 * Class BAvgTermFactMfo
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BAvgTermFactMfo $model
 */
class BAvgTermFactMfo extends AWorker
{
    use ValidateCreditDlflStat;
    use GetAttribute;
    use ArrayStringToLower;
    use GetLastCreditItem;
    use GetDealLife;
    use NormalizeData;
    use GetCrDeal;
    use ConvertDate;

    /**
     *
     */
    protected function initScore(): void
    {
        $crDeal = $this->getCrDeal($this->model->bkiFile);

        if (null === $crDeal) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);
        $data = [];

        foreach ($crDeal as $credit) {
            if (!$this->validateStat($credit, $this->model->dlFlStat) ||
                !in_array(
                    $this->getAttribute('dldonor', $credit, null, true),
                    $this->arrayStringToLower($this->model->donor),
                    true
                )) {
                Logger::log([
                    'dldonor' => $this->getAttribute('dldonor', $credit),
                    'donor' => $this->arrayStringToLower($this->model->donor)
                ], Logger::LOG_LEVEL_INFO);
                continue;
            }
            $last = $this->getLastCreditItem($credit);
            $dldff = $this->getAttribute('dldff', $last);
            $dlds = $this->getAttribute('dlds', $last);

            Logger::log([
                'dldff' => $dldff,
                'dlds' => $dlds
            ], Logger::LOG_LEVEL_ALL);

            try {
                if ((false === strtotime($dldff)) || (false === strtotime($dlds))) {
                    continue;
                }

                $dldff = new \DateTime($dldff, new \DateTimeZone('Europe/Kiev'));
                $dlds = new \DateTime($dlds, new \DateTimeZone('Europe/Kiev'));

                $sub = BcMathFactory::instance()->bcsub($dldff->getTimestamp(), $dlds->getTimestamp());
                $day = $this->convertSecondToDay($sub);
                $data[] = $day;
                Logger::log([
                    '$sub' => $sub,
                    '$day' => $day,
                    'add' => true
                ], Logger::LOG_LEVEL_INFO);
            } catch (\Throwable $e) {
                Logger::logError($e, Logger::LOG_LEVEL_ALL);
            }
        }

        if (count($data)) {
            $this->score = BcMathFactory::instance()->bcdiv(array_sum($data), count($data));
        }
    }
}
