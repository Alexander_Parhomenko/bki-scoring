<?php

namespace ifinance\scoring\v1\workers\traits;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\Logger;

/**
 * Trait ValidateDlamt
 * @package ifinance\scoring\v1\workers\traits
 */
trait ValidateDlamt
{
    /**
     * @param string $dlamt
     * @return bool
     */
    public function validateDlamt(?string $dlamt): bool
    {
        try {
            $status = (strtolower($dlamt) === 'null' ||
                $dlamt === null ||
                $dlamt === '' ||
                BcMathFactory::instance()->bccomp($dlamt, 0) === 0);
        } catch (\Throwable $e) {
            Logger::log($e, Logger::LOG_LEVEL_INFO);
            $status = true;
        } finally {
            return !$status;
        }
    }
}
