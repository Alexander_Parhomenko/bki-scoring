<?php

namespace ifinance\scoring\v1\workers\traits;

use ifinance\scoring\interfaces\Logger;

/**
 * Trait ValidateOrg
 * @package ifinance\scoring\v1\workers\traits
 */
trait ValidateOrg
{
    /**
     * @param array $credresItem
     * @param array $org
     * @return bool
     */
    public function validateOrg(array $credresItem, array $org): bool
    {
        $res = $this->getAttribute('org', $credresItem, null, true);
        $org = $this->arrayStringToLower($org);
        Logger::log([
            'res' => $res,
            'orgList' => $org,
        ], Logger::LOG_LEVEL_ALL);
        return in_array($res, $org);
    }

    /**
     * @param string $name
     * @param array|null $array
     * @param null $defaultValue
     * @param bool $strToLower
     * @return mixed
     */
    abstract public function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false);

    /**
     * @param array $data
     * @return array
     */
    abstract public function arrayStringToLower(array $data): array;
}
