<?php

namespace ifinance\scoring\v1\workers\traits;

use ifinance\scoring\v1\Registry;

/**
 * Trait GetLastCreditItem
 * @package ifinance\scoring\v1\workers\traits
 */
trait GetLastCreditItem
{
    /**
     * @param array|null $credit
     * @return array|null
     */
    public function getLastCreditItem(?array $credit): ?array
    {
        $key = base64_encode(__FUNCTION__ . json_encode($credit));
        if (Registry::isExist($key)) {
            return Registry::get($key);
        }
        $dealLife = $this->getDealLife($credit);
        if (null === $dealLife) {
            return null;
        }
        $dealLife = $this->normalizeData($dealLife);

        $statYear = null;
        $statMonth = null;
        $itemResult = null;
        foreach ($dealLife as $item) {
            $dlYear = $this->getAttribute('dlyear', $item);
            $dlMonth = $this->getAttribute('dlmonth', $item);
            if ((null === $statYear && $dlYear !== null && $dlMonth !== null) ||
                ($dlYear > $statYear) ||
                ($dlYear == $statYear && $dlMonth >= $statMonth)
            ) {
                $statYear = $dlYear;
                $statMonth = $dlMonth;
                $itemResult = $item;
            }
        }

        Registry::add($key, $itemResult);
        return $itemResult;
    }

    /**
     * @param array|null $credit
     * @return array|null
     */
    abstract public function getDealLife(?array $credit): ?array;

    /**
     * @param array $data
     * @return array
     */
    abstract public function normalizeData(array $data): array;

    /**
     * @param string $name
     * @param array|null $array
     * @param null $defaultValue
     * @param bool $strToLower
     * @return mixed
     */
    abstract public function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false);
}
