<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait ValidateDlamtexps
 * @package ifinance\scoring\v1\workers\traits
 */
trait ValidateDlamtexps
{
    /**
     * @param array $credit
     * @param $validateMinDlamtExp
     * @return bool
     */
    public function validateDlamtexps(array $credit, $validateMinDlamtExp): bool
    {
        $item = $this->getLastCreditItem($credit);
        return $this->validateDlamtexp($item, $validateMinDlamtExp, false);
    }

    /**
     * @param array|null $creditItem
     * @param $validate
     * @param bool $isMax
     * @param bool $isEqually
     * @return bool
     */
    abstract public function validateDlamtexp(
        ?array $creditItem,
        $validate,
        $isMax = true,
        bool $isEqually = false
    ): bool;

    /**
     * @param array|null $credit
     * @return array|null
     */
    abstract public function getLastCreditItem(?array $credit): ?array;
}
