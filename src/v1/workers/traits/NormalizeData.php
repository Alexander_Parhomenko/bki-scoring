<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait NormalizeData
 * @package ifinance\scoring\v1\workers\traits
 */
trait NormalizeData
{
    /**
     * @param array $data
     * @return array
     */
    public function normalizeData(array $data): array
    {
        if (empty($data['@attributes'])) {
            return $data;
        }

        return [$data];
    }
}
