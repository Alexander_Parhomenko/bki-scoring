<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait GetCrDeal
 * @package ifinance\scoring\v1\workers\traits
 */
trait GetCrDeal
{
    /**
     * @param array|null $report
     * @return array|null
     */
    public function getCrDeal(?array $report): ?array
    {
        if (null === $report) {
            return null;
        }

        $crDeal = $report['creditHistory']['crdeal'] ?? null;

        if (null === $crDeal || !is_array($crDeal) || !count($crDeal)) {
            return null;
        }

        $crDeal = $this->normalize($crDeal);

        return $crDeal;
    }

    /**
     * @param array $crDeal
     * @return array
     */
    private function normalize(array $crDeal): array
    {
        $normalizedCrDeal = [];

        $crDeal = $this->normalizeData($crDeal);

        foreach ($crDeal as $credit) {
            $dlref = $this->getAttribute('dlref', $credit);
            $dealLife = $this->normalizeData($this->getAttribute('deallife', $credit));

            if (!array_key_exists($dlref, $normalizedCrDeal)) {
                $normalizedCrDeal[$dlref] = $credit;
                $normalizedCrDeal[$dlref]['deallife'] = $dealLife;
            } else {
                $normalizedCrDeal[$dlref]['@attributes'] = $credit['@attributes'];
                $normalizedCrDeal[$dlref]['deallife'] = array_merge($normalizedCrDeal[$dlref]['deallife'], $dealLife);
            }
        }

        return array_values($normalizedCrDeal);
    }

    /**
     * @param array $data
     * @return array
     */
    abstract public function normalizeData(array $data): array;

    /**
     * @param string $name
     * @param array|null $array
     * @param null $defaultValue
     * @param bool $strToLower
     * @return mixed
     */
    abstract protected function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false);
}
