<?php

namespace ifinance\scoring\v1\workers\traits;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\Logger;

/**
 * Trait ValidateDldateclc
 * @package ifinance\scoring\v1\workers\traits
 */
trait ValidateDldateclc
{
    /**
     * @param array $credit
     * @param int $created
     * @param $validateMaxDldateClc
     * @return bool
     */
    public function validateDldateclc(array $credit, int $created, $validateMaxDldateClc): bool
    {
        $item = $this->getLastCreditItem($credit);
        $dldateclc = $this->getAttribute('dldateclc', $item);
        try {
            return abs(BcMathFactory::instance()->bcsub(strtotime($dldateclc), $created))
                <= strval($validateMaxDldateClc);
        } catch (\Throwable $e) {
            Logger::log($e, Logger::LOG_LEVEL_CRITICAL);
            return false;
        }
    }

    /**
     * @param array|null $credit
     * @return array|null
     */
    abstract public function getLastCreditItem(?array $credit): ?array;

    /**
     * @param string $name
     * @param array|null $array
     * @param null $defaultValue
     * @param bool $strToLower
     * @return mixed
     */
    abstract public function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false);
}
