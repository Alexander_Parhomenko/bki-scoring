<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait ConvertDate
 * @package ifinance\scoring\v1\workers\traits
 */
trait ConvertDate
{
    public function convertDayToSecond(int $date): int
    {
        return 24 * 60 * 60 * $date;
    }

    public function convertSecondToDay(int $second): int
    {
        return (int) round($second / (24 * 60 * 60));
    }

    public function roundingDateToDay(int $timestamp): int
    {
        $date = new \DateTime('@' . $timestamp);
        $date->modify('00:00:00.000000');

        return $date->getTimestamp();
    }

    public function roundingDateToDayByTimezone(int $timestamp, string $timezone)
    {
        $date = new \DateTime('@' . $timestamp);
        $date->setTimezone(new \DateTimeZone($timezone));
        $date->modify('00:00:00.000000');

        return $date->getTimestamp();
    }
}
