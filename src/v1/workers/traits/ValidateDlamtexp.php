<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait ValidateDlamtexp
 * @package ifinance\scoring\v1\workers\traits
 */
trait ValidateDlamtexp
{
    /**
     * @param array|null $creditItem
     * @param $validate
     * @param bool $isMax
     * @param bool $isEqually
     * @return bool
     */
    public function validateDlamtexp(?array $creditItem, $validate, $isMax = true, bool $isEqually = false): bool
    {
        if (!is_array($creditItem)) {
            return false;
        }
        $dlamtexp = $this->getAttribute('dlamtexp', $creditItem);
        if ($isMax) {
            if ($isEqually) {
                return $dlamtexp <= $validate;
            } else {
                return $dlamtexp < $validate;
            }
        } else {
            if ($isEqually) {
                return $dlamtexp >= $validate;
            } else {
                return $dlamtexp > $validate;
            }
        }
    }

    /**
     * @param string $name
     * @param array|null $array
     * @param null $defaultValue
     * @param bool $strToLower
     * @return mixed
     */
    abstract public function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false);
}
