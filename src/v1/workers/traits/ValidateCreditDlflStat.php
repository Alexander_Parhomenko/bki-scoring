<?php

namespace ifinance\scoring\v1\workers\traits;

use ifinance\scoring\interfaces\Logger;

/**
 * Trait ValidateCreditDlflStat
 * @package ifinance\scoring\v1\workers\traits
 */
trait ValidateCreditDlflStat
{
    /**
     * @param array|null $credit
     * @param array|null $validateStat
     * @return bool
     */
    public function validateStat(?array $credit, ?array $validateStat): bool
    {
        if (!is_array($credit) || !is_array($validateStat)) {
            return false;
        }
        $item = $this->getLastCreditItem($credit);
        $stat = $this->getAttribute('dlflstat', $item, null, true);
        $validateStat = $this->arrayStringToLower($validateStat);
        Logger::log(
            [
                '$stat' => $stat,
                '$validateStat' => $validateStat,
                'result' => in_array((string)$stat, $validateStat)
            ],
            Logger::LOG_LEVEL_ALL
        );
        return in_array((string)$stat, $validateStat);
    }

    /**
     * @param array|null $credit
     * @return array|null
     */
    abstract public function getLastCreditItem(?array $credit): ?array;

    /**
     * @param array $data
     * @return array
     */
    abstract public function arrayStringToLower(array $data): array;
}
