<?php

namespace ifinance\scoring\v1\workers\traits;

use ifinance\scoring\interfaces\Logger;

/**
 * Trait ValidateRedate
 * @package ifinance\scoring\v1\workers\traits
 */
trait ValidateRedate
{
    /**
     * @param array $credresItem
     * @param $created
     * @param $maxRedate
     * @return bool
     */
    public function validateRedate(array $credresItem, $created, $maxRedate): bool
    {
        $redate = $this->getAttribute('redate', $credresItem);
        if (null === $redate) {
            return false;
        }
        $redate = strtotime($redate);

        Logger::log([
            'redate' => $redate,
            'created' => $created,
            'sub' => $created - $redate,
            'maxRedate' => $maxRedate,
        ], Logger::LOG_LEVEL_ALL);

        return ($created - $redate) < $maxRedate;
    }

    /**
     * @param string $name
     * @param array|null $array
     * @param null $defaultValue
     * @param bool $strToLower
     * @return mixed
     */
    abstract public function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false);
}
