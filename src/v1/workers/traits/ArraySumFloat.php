<?php

namespace ifinance\scoring\v1\workers\traits;

use DrLenux\BcMath\BcMathFactory;

/**
 * Trait ArraySumFloat
 * @package ifinance\scoring\v1\workers\traits
 */
trait ArraySumFloat
{
    /**
     * @param array $data
     * @return string|null
     */
    public function arraySumFloat(array $data): ?string
    {
        $res = '0';
        foreach ($data as $item) {
            try {
                $res = BcMathFactory::instance()->bcadd($res, $item);
            } catch (\Throwable $e) {
                return null;
            }
        }
        return $res;
    }
}
