<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait GetAttribute
 * @package ifinance\scoring\v1\workers\traits
 */
trait GetAttribute
{
    /**
     * @param string $name
     * @param array|null $array
     * @param null $defaultValue
     * @param bool $strToLower
     * @return mixed|string|null
     */
    public function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false)
    {
        if (!is_array($array) || !count($array)) {
            return $defaultValue;
        }
        $res = $array[$name] ?? $array['@attributes'][$name] ?? $defaultValue;

        if (is_string($res)) {
            $res = trim($res);
            if ($strToLower) {
                $res = strtolower($res);
            }
        }
        return $res;
    }
}
