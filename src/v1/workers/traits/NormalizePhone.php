<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait NormalizePhone
 * @package ifinance\scoring\v1\workers\traits
 */
trait NormalizePhone
{
    /**
     * @param array $phones
     */
    public function normalizePhoneNumbers(array &$phones)
    {
        array_walk($phones, function (&$item) {
            $item = self::normalizePhoneNumber($item);
        });
        krsort($phones);
        $phones = array_values($phones);
        $phones = array_unique($phones);
    }

    /**
     * @param string $phone
     * @return string
     */
    public function normalizePhoneNumber(string $phone): string
    {
        return substr($phone, -10); // last 10
    }
}
