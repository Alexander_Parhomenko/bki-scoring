<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait GetDealLife
 * @package ifinance\scoring\v1\workers\traits
 */
trait GetDealLife
{
    /**
     * @param array|null $credit
     * @return array|null
     */
    public function getDealLife(?array $credit): ?array
    {
        if (!is_array($credit)) {
            return null;
        }
        $dealLife = $this->getAttribute('deallife', $credit);
        if (null === $dealLife || !is_array($dealLife) ?? !count($dealLife)) {
            return null;
        }
        return $dealLife;
    }

    abstract public function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false);
}
