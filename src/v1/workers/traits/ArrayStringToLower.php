<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait ArrayStringToLower
 * @package ifinance\scoring\v1\workers\traits
 */
trait ArrayStringToLower
{
    /**
     * @param array $data
     * @return array
     */
    public function arrayStringToLower(array $data): array
    {
        foreach ($data as &$item) {
            if (is_string($item)) {
                $item = strtolower(trim($item));
            }
        }
        return $data;
    }
}
