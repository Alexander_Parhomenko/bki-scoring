<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait ValidateDlCelCred
 * @package ifinance\scoring\v1\workers\traits
 */
trait ValidateDlCelCred
{
    /**
     * @param array $crDealItem
     * @param array $validate
     * @return bool
     */
    public function validateDlCelCred(array $crDealItem, array $validate): bool
    {
        $validate = $this->arrayStringToLower($validate);
        $dlCelCred = $this->getAttribute('dlcelcred', $crDealItem, null, true);

        return in_array($dlCelCred, $validate, true);
    }

    /**
     * @param string $name
     * @param array|null $array
     * @param null $defaultValue
     * @param bool $strToLower
     * @return mixed
     */
    abstract public function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false);

    /**
     * @param array $data
     * @return array
     */
    abstract public function arrayStringToLower(array $data): array;
}
