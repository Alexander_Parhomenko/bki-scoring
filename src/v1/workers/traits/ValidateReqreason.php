<?php

namespace ifinance\scoring\v1\workers\traits;

/**
 * Trait ValidateReqreason
 * @package ifinance\scoring\v1\workers\traits
 */
trait ValidateReqreason
{
    /**
     * @param array $credresItem
     * @return bool
     */
    public function validateReqreason(array $credresItem, $reqReason): bool
    {
        $reqreason = strtoupper($this->getAttribute('reqreason', $credresItem));
        return in_array($reqreason, $reqReason);
    }

    /**
     * @param string $name
     * @param array|null $array
     * @param null $defaultValue
     * @param bool $strToLower
     * @return mixed
     */
    abstract public function getAttribute(string $name, ?array $array, $defaultValue = null, $strToLower = false);
}
