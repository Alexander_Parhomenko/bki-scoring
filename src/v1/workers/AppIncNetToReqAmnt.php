<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;

/**
 * Class AppIncNetToReqAmnt
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\AppIncNetToReqAmnt $model
 */
class AppIncNetToReqAmnt extends AWorker
{
    /**
     *
     */
    protected function initScore(): void
    {
        $this->score = 0;
        try {
            $sub = BcMathFactory::instance()->bcsub($this->model->income, $this->model->expences);
            $sub = BcMathFactory::instance()->bcsub($sub, $this->model->otherCreditPayes);
            $this->score = BcMathFactory::instance()->bcdiv($sub, $this->model->orderSum);
        } catch (\Throwable $e) {
            Logger::log([
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ], Logger::LOG_LEVEL_CRITICAL);
        }
    }
}
