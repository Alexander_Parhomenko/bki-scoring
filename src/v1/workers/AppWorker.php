<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\v1\models\AppModel;

/**
 * Class AppWorker
 * @package ifinance\scoring\v1\workers
 *
 * @property AppModel $model
 */
class AppWorker extends AWorker
{
    /**
     *
     */
    protected function initScore(): void
    {
        $this->score = $this->model->score;
    }
}
