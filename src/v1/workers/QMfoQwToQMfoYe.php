<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;

/**
 * Class QMfoQwToQMfoYe
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\QMfoQwToQMfoYe $model
 */
class QMfoQwToQMfoYe extends AWorker
{
    /**
     *
     */
    protected function initScore(): void
    {
        $this->score = (float)BcMathFactory::instance()->bcdiv(
            $this->model->qMfoQw,
            $this->model->qMfoYe
        );
    }
}
