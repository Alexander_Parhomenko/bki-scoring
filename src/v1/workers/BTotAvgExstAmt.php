<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\ArraySumFloat;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetFirstCreditItem;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;

/**
 * Class BTotAvgExstAmt
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BTotAvgExstAmt $model
 */
class BTotAvgExstAmt extends AWorker
{
    use GetCrDeal;
    use NormalizeData;
    use ValidateCreditDlflStat;
    use GetLastCreditItem;
    use GetAttribute;
    use ArrayStringToLower;
    use GetDealLife;
    use ValidateDlamt;
    use GetFirstCreditItem;
    use ArraySumFloat;

    /**
     *
     */
    protected function initScore(): void
    {
        $crDeal = $this->getCrDeal($this->model->bkiFile);

        if (null === $crDeal) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);

        $avg = [];

        foreach ($crDeal as $credit) {
            $validateStat = $this->validateStat($credit, $this->model->dlFlStat);
            if ($validateStat) {
                $dlamt = $this->getAttribute('dlamt', $credit);
                Logger::log(['dlamt' => $dlamt], Logger::LOG_LEVEL_ALL);
                if ($this->validateDlamt($dlamt)) {
                    $avg[] = $dlamt;
                } else {
                    $first = $this->getFirstCreditItem($credit);
                    $dlamtCur = $this->getAttribute('dlamtcur', $first);
                    Logger::log(['dlamtcur' => $dlamtCur], Logger::LOG_LEVEL_ALL);
                    try {
                        BcMathFactory::instance()->bccomp($dlamtCur, 0);
                        $avg[] = $dlamtCur;
                    } catch (\Throwable $e) {
                    }
                }
            }
        }
        Logger::log([
            'avg' => $avg
        ], Logger::LOG_LEVEL_ALL);
        $sum = $this->arraySumFloat($avg);

        try {
            if (count($avg)) {
                $this->score = BcMathFactory::instance()->bcdiv($sum, count($avg));
            }
        } catch (\Throwable $e) {
            Logger::logError($e, Logger::LOG_LEVEL_INFO);
            $this->score = null;
        }
    }
}
