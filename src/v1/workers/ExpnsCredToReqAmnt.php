<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;

/**
 * Class ExpnsCredToReqAmnt
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\ExpnsCredToReqAmnt $model
 */
class ExpnsCredToReqAmnt extends AWorker
{
    /**
     *
     */
    protected function initScore(): void
    {
        $this->score = BcMathFactory::instance()->bcdiv(
            $this->model->userOtherCreditPayes,
            $this->model->requestCreditSum
        );
    }
}
