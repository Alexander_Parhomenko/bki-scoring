<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;

/**
 * Class BTotCntCur7DpdExstLns
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BTotCntCur7DpdExstLns $model
 */
class BTotCntCur7DpdExstLns extends AWorker
{
    use GetAttribute;
    use NormalizeData;
    use ValidateCreditDlflStat;
    use ArrayStringToLower;
    use GetCrDeal;
    use GetDealLife;
    use GetLastCreditItem;

    /**
     *
     */
    protected function initScore(): void
    {
        $crDeal = $this->getCrDeal($this->model->bkiFile);

        if (null === $crDeal) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);

        foreach ($crDeal as $credit) {
            if (null === $this->score) {
                $this->score = 0;
            }
            $validateStat = $this->validateStat($credit, $this->model->dlFlStat);
            if ($validateStat) {
                $last = $this->getLastCreditItem($credit);
                $dldayexp = $this->getAttribute('dldayexp', $last);
                try {
                    Logger::log(['$dldayexp' => $dldayexp], Logger::LOG_LEVEL_ALL);
                    if (BcMathFactory::instance()->bccomp($dldayexp, $this->model->minDlDayExp) >= 0) {
                        $this->score++;
                    }
                } catch (\Throwable $e) {
                    Logger::logError($e, Logger::LOG_LEVEL_INFO);
                }
            }
        }
    }
}
