<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;
use ifinance\scoring\v1\workers\traits\ValidateDlCelCred;

/**
 * Class BTotSumcExstBalCmr
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BTotSumcExstBalCmr $model
 */
class BTotSumcExstBalCmr extends AWorker
{
    use NormalizeData;
    use GetCrDeal;
    use GetLastCreditItem;
    use GetAttribute;
    use GetDealLife;
    use ValidateCreditDlflStat;
    use ValidateDlCelCred;
    use ArrayStringToLower;

    /**
     *
     */
    protected function initScore(): void
    {
        $report = $this->model->bkiFile;
        $crDeal = $this->getCrDeal($report);

        if (null === $crDeal || !count($crDeal)) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);
        $this->score = null;

        foreach ($crDeal as $credit) {
            $validateDlCelCred = $this->validateDlCelCred($credit, $this->model->dlcelcred);
            $validateStat = $this->validateStat($credit, $this->model->dlflstat);
            Logger::log([
                '$validateDlCelCred' => $validateDlCelCred,
                '$validateStat' => $validateStat
            ], Logger::LOG_LEVEL_INFO);

            if ($validateDlCelCred && $validateStat) {
                $lastItem = $this->getLastCreditItem($credit);
                $dlamtCur = $this->getAttribute('dlamtcur', $lastItem);
                Logger::log(['$dlamtCur' => $dlamtCur], Logger::LOG_LEVEL_INFO);
                try {
                    $this->score = BcMathFactory::instance()->bcadd((float) $this->score, $dlamtCur);
                } catch (\Throwable $e) {
                    Logger::log($e, Logger::LOG_LEVEL_INFO);
                }
            }
        }

        if (null !== $this->score) {
            $this->score = (float) $this->score;
        }
    }
}
