<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;

/**
 * Class PreviousMaxAmountToRequestedAmount
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\PreviousMaxAmountToRequestedAmount $model
 */
class PreviousMaxAmountToRequestedAmount extends AWorker
{
    /**
     *
     */
    protected function initScore(): void
    {
        $this->score = BcMathFactory::instance()->bcdiv(
            $this->model->maxCreditSumApprove,
            $this->model->requestCreditSum
        );
    }
}
