<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;

/**
 * Class BTotCntClsdLns
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BTotCntClsdLns $model
 */
class BTotCntClsdLns extends AWorker
{
    use GetCrDeal;
    use ValidateCreditDlflStat;
    use GetLastCreditItem;
    use GetDealLife;
    use GetAttribute;
    use NormalizeData;
    use ArrayStringToLower;

    /**
     *
     */
    protected function initScore(): void
    {
        $report = $this->model->bkiFile;
        $crDeal = $this->getCrDeal($report);

        if (null === $crDeal) {
            return;
        }

        $this->score = 0;

        $crDeal = $this->normalizeData($crDeal);
        foreach ($crDeal as $credit) {
            $validateStat = $this->validateStat($credit, $this->model->dlflstat);
            Logger::log([
                '$validateStat' => $validateStat
            ], Logger::LOG_LEVEL_INFO);
            if ($validateStat) {
                $this->score++;
            }
        }
    }
}
