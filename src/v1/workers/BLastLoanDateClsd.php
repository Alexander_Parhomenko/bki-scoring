<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use ifinance\scoring\v1\workers\traits\ConvertDate;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateCreditDlflStat;

/**
 * Class BLastLoanDateClsd
 * @package ifinance\scoring\v1\workers\
 *
 * @property \ifinance\scoring\v1\models\BLastLoanDateClsd $model
 */
class BLastLoanDateClsd extends AWorker
{
    use GetCrDeal;
    use NormalizeData;
    use ValidateCreditDlflStat;
    use GetAttribute;
    use GetLastCreditItem;
    use GetDealLife;
    use ArrayStringToLower;
    use ConvertDate;

    /**
     *
     */
    protected function initScore(): void
    {
        $crDeal = $this->getCrDeal($this->model->bkiFile);
        $crDeal = $this->normalizeData($crDeal);

        $dlds = 0;
        foreach ($crDeal as $credit) {
            if ($this->validateStat($credit, $this->model->dlflstat)) {
                $dealLife = $this->getDealLife($credit);
                $dealLife = $this->normalizeData($dealLife);
                foreach ($dealLife as $creditItem) {
                    try {
                        $currentDlDs = new \DateTime($this->getAttribute('dlds', $creditItem), new \DateTimeZone('Europe/Kiev'));
                        Logger::log(
                            [
                                '$currentDlDs' => $currentDlDs,
                                '$dlds' => $dlds
                            ],
                            Logger::LOG_LEVEL_INFO
                        );
                        $currentDlDs = $currentDlDs->getTimestamp();
                        $dlds = max($dlds, $currentDlDs);
                    } catch (\Exception $e) {
                        Logger::log(
                            [
                                'exception' => $e,
                                'dlds' => $dlds
                            ],
                            Logger::LOG_LEVEL_INFO
                        );
                    }
                }
            }
        }
        if ($dlds !== 0) {
            $created = $this->roundingDateToDayByTimezone($this->model->orderCreated, 'Europe/Kiev');

            $div = $created - $dlds;
            $this->score = (float) $this->convertSecondToDay($div);
        }
    }
}
