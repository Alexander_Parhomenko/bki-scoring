<?php

namespace ifinance\scoring\v1\workers;

use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\NormalizeData;

/**
 * Class BkiAplEmailMatchOrAplIsEmailConfirmed
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BkiAplEmailMatchOrAplIsEmailConfirmed $model
 */
class BkiAplEmailMatchOrAplIsEmailConfirmed extends AWorker
{
    use NormalizeData;
    use GetAttribute;

    const EMAIL_VERIFY_OK = 1;
    const EMAIL_MATCH = 2;
    const OTHER = null;

    /**
     *
     */
    protected function initScore(): void
    {
//        if ($this->model->emailValidate) {
//            $this->score = self::EMAIL_VERIFY_OK;
//            return;
//        }
//
//        $report = $this->model->bkiFile;
//        $contactHistory = $report['contactHistory'] ?? [];
//        $cont = $contactHistory['cont'] ?? [];
//
//        if (!count($cont)) {
//            return;
//        }
//
//        $cont = $this->normalizeData($cont);
//        $email = strtolower($this->model->email);
//
//        foreach ($cont as $item) {
//            if (in_array($this->getAttribute('ctype', $item), $this->model->cType, true)) {
//                $cVal = $this->getAttribute('cval', $item, null, true);
//                if ($email === $cVal) {
//                    $this->score = self::EMAIL_MATCH;
//                    return;
//                }
//            }
//        }
//        $this->score = self::OTHER;

        $this->score = 0;
    }
}
