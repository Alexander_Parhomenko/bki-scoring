<?php

namespace ifinance\scoring\v1\workers;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\interfaces\AWorker;
use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\v1\workers\traits\GetAttribute;
use ifinance\scoring\v1\workers\traits\GetCrDeal;
use ifinance\scoring\v1\workers\traits\GetDealLife;
use ifinance\scoring\v1\workers\traits\GetLastCreditItem;
use ifinance\scoring\v1\workers\traits\NormalizeData;
use ifinance\scoring\v1\workers\traits\ValidateDlamt;

/**
 * Class BLastLoanStatus
 * @package ifinance\scoring\v1\workers
 *
 * @property \ifinance\scoring\v1\models\BLastLoanStatus $model
 */
class BLastLoanStatus extends AWorker
{
    use GetCrDeal;
    use GetLastCreditItem;
    use GetAttribute;
    use NormalizeData;
    use GetDealLife;
    use ValidateDlamt;

    /**
     *
     */
    protected function initScore(): void
    {
        $crDeal = $this->getCrDeal($this->model->bkiFile);

        if (null === $crDeal) {
            return;
        }

        $crDeal = $this->normalizeData($crDeal);

        $maxDlDs = 0;

        foreach ($crDeal as $id => $credit) {
            $lastItem = $this->getLastCreditItem($credit);
            $allDlDs = [];
            foreach ($credit['deallife'] as $creditItem) {
                $allDlDs[] = $this->getAttribute('dlds', $creditItem);
            }
            $dlds = max($allDlDs);
            $dlflstat = $this->getAttribute('dlflstat', $lastItem);
            Logger::log([
                '$id' => $id,
                '$dlds' => $dlds,
                'dlflstat' => $dlflstat
            ], Logger::LOG_LEVEL_ALL);
            try {
                $dlds = strtotime($dlds);
                if (BcMathFactory::instance()->bccomp($maxDlDs, $dlds) <= 0 &&
                    $this->validateDlamt($dlflstat)
                ) {
                    $maxDlDs = $dlds;
                    $this->score = $dlflstat;
                }
            } catch (\Throwable $e) {
                Logger::logError($e, Logger::LOG_LEVEL_INFO);
            }
        }

        if (null !== $this->score) {
            $this->score = (float)$this->score;
        }
    }
}
