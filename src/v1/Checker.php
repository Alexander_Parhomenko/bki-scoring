<?php

namespace ifinance\scoring\v1;

use DrLenux\BcMath\BcMathFactory;
use ifinance\scoring\exceptions\NotValidZoneDiapason;
use ifinance\scoring\interfaces\Logger;
use yii\base\BaseObject;

/**
 * Class Checker
 * @package ifinance\scoring\v1
 */
class Checker extends BaseObject
{
    /**
     * @var ?float
     */
    public $min;

    /**
     * @var ?float
     */
    public $max;

    /**
     * @throws NotValidZoneDiapason
     */
    public function init()
    {
        try {
            $this->validate($this->min);
            $this->validate($this->max);
        } catch (\Throwable $e) {
            throw new NotValidZoneDiapason($e->getMessage());
        }
    }

    /**
     * @param float|null $value
     */
    private function validate(?float $value)
    {
        if (null !== $value) {
            BcMathFactory::instance()->bccomp($value, 0);
        }
    }

    /**
     * @param float|null $score
     * @return bool
     */
    public function check(?float $score): bool
    {
        try {
            if ($this->min === $this->max) {
                if (null === $this->min) {
                    return $score === $this->min;
                }
                try {
                    return BcMathFactory::instance()->bccomp($score, $this->min) === 0;
                } catch (\Throwable $e) {
                    Logger::logError($e, Logger::LOG_LEVEL_INFO);
                    return false;
                }
            }

            $valid = true;

            if (null !== $this->min) {
                $valid = $valid && (BcMathFactory::instance()->bccomp($score, $this->min) >= 0);
            }

            if (null !== $this->max) {
                $valid = $valid && (BcMathFactory::instance()->bccomp($score, $this->max) < 0);
            }

            return $valid;
        } catch (\Throwable $e) {
            Logger::logError($e, Logger::LOG_LEVEL_INFO);
            return false;
        }
    }
}
