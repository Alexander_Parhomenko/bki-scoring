<?php

use ifinance\scoring\v1\workers;
use ifinance\scoring\v1\models;

return [
    'q_mfo_ye' => [
        'worker' => workers\QMfoYe::class,
        'model' => models\QMfoYe::class
    ],
    'b_mfo_avg_exst_amt' => [
        'worker' => workers\BMfoAvgExstAmt::class,
        'model' => models\BMfoAvgExstAmt::class
    ],
    'b_mfo_sum_clsd_amt' => [
        'worker' => workers\BMfoSumClsdAmt::class,
        'model' => models\BMfoSumClsdAmt::class
    ],
    'b_mfo_maxc_dpd_exst_lns_100_60' => [
        'worker' => workers\BTotMaxcDpdExstLns100And60::class,
        'model' => models\BTotMaxcDpdExstLns100And60::class
    ],
    'b_tot_sumc_exst_bal_cmr' => [
        'worker' => workers\BTotSumcExstBalCmr::class,
        'model' => models\BTotSumcExstBalCmr::class
    ],
    'b_tot_cnt_clsd_lns' => [
        'worker' => workers\BTotCntClsdLns::class,
        'model' => models\BTotCntClsdLns::class
    ],
    'b_tot_avg_tot_amt' => [
        'worker' => workers\BTotAvgTotAmt::class,
        'model' => models\BTotAvgTotAmt::class
    ],
    'b_tot_max_dpd_tot_lns_100' => [
        'worker' => workers\BTotMaxDpdTotLns100::class,
        'model' => models\BTotMaxDpdTotLns100::class
    ],
    'b_last_loan_date_clsd' => [
        'worker' => workers\BLastLoanDateClsd::class,
        'model' => models\BLastLoanDateClsd::class
    ],
    'b_last_loan_status' => [
        'worker' => workers\BLastLoanStatus::class,
        'model' => models\BLastLoanStatus::class
    ],
    'b_mfo_cnt_sold_12m_lns' => [
        'worker' => workers\BMfoCntSold12mLns::class,
        'model' => models\BMfoCntSold12mLns::class
    ],
    'bki_apl_email_match_or_apl_is_email_confirmed' => [
        'worker' => workers\BkiAplEmailMatchOrAplIsEmailConfirmed::class,
        'model' => models\BkiAplEmailMatchOrAplIsEmailConfirmed::class
    ],
    'bki_apl_email_match_bki_apl_phone_match' => [
        'worker' => workers\BkiAplEmailMatchBkiAplPhoneMatch::class,
        'model' => models\BkiAplEmailMatchBkiAplPhoneMatch::class
    ],
    'bki_education_agr' => [
        'worker' => workers\BkiEducationAgr::class,
        'model' => models\BkiEducationAgr::class
    ],
    'score_actual' => [
        'worker' => workers\ScoreActual::class,
        'model' => models\ScoreActual::class
    ],
    'score_vodafone' => [
        'worker' => workers\ScoreVodafone::class,
        'model' => models\ScoreVodafone::class
    ],
    'score_kyivstar_ks_numbers' => [
        'worker' => workers\ScoreKyivstarKSNumbers::class,
        'model' => models\ScoreKyivstarKSNumbers::class
    ],
    'score_kyivstar_non_ks_numbers' => [
        'worker' => workers\ScoreKyivstarNonKSNumbers::class,
        'model' => models\ScoreKyivstarNonKSNumbers::class
    ],
    'score_artellence' => [
        'worker' => workers\ScoreArtellence::class,
        'model' => models\ScoreArtellence::class
    ],
    'app_education_type' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'app_education_type'
        ]
    ],
    'app_industry_type_agr' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'app_industry_type_agr'
        ]
    ],
    'app_requested_period' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'app_requested_period'
        ]
    ],
    'days_registered' => [
        'worker' => workers\DaysRegistered::class,
        'model' => models\DaysRegistered::class
    ],
    'app_fb' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'app_fb'
        ]
    ],
    'app_inc_net_to_req_amnt' => [
        'worker' => workers\AppIncNetToReqAmnt::class,
        'model' => models\AppIncNetToReqAmnt::class
    ],
    'app_type_residance' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'app_type_residance'
        ]
    ],
    'q_mfo_wk' => [
        'worker' => workers\QMfoWk::class,
        'model' => models\QMfoWk::class,
    ],
    'b_tot_avg_exst_amt' => [
        'worker' => workers\BTotAvgExstAmt::class,
        'model' => models\BTotAvgExstAmt::class,
    ],
    'b_tot_sum_clsd_amt' => [
        'worker' => workers\BTotSumClsdAmt::class,
        'model' => models\BTotSumClsdAmt::class,
    ],
    'b_tot_max_ovrd_clsd_lns' => [
        'worker' => workers\BTotMaxOvrdClsdLns::class,
        'model' => models\BTotMaxOvrdClsdLns::class
    ],
    'b_tot_cnt_cur7dpd_exst_lns' => [
        'worker' => workers\BTotCntCur7DpdExstLns::class,
        'model' => models\BTotCntCur7DpdExstLns::class,
    ],
    'old_bki_apl_email_match_bki_apl_phone_match' => [
        'worker' => workers\OldBkiAplEmailMatchBkiAplPhoneMatch::class,
        'model' => models\OldBkiAplEmailMatchBkiAplPhoneMatch::class,
    ],
    'bki_apl_count_of_adress_param_match' => [
        'worker' => workers\BkiAplCountOfAdressParamMatch::class,
        'model' => models\BkiAplCountOfAdressParamMatch::class
    ],
    'q_mfo_qw' => [
        'worker' => workers\QMfoQw::class,
        'model' => models\QMfoQw::class
    ],
    'q_mfo_qw_to_q_mfo_ye' => [
        'worker' => workers\QMfoQwToQMfoYe::class,
        'model' => models\QMfoQwToQMfoYe::class,
        'depends' => [
            'q_mfo_ye',
            'q_mfo_qw'
        ]
    ],
    'b_avg_term_fact_mfo' => [
        'worker' => workers\BAvgTermFactMfo::class,
        'model' => models\BAvgTermFactMfo::class,
    ],
    'apl_from_last_acpt' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'apl_from_last_acpt'
        ]
    ],
    'app_employment_type' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'app_employment_type'
        ]
    ],
    'app_family_status' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'app_family_status'
        ]
    ],
    'app_position' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'app_position'
        ]
    ],
    'beh_client_reject' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'beh_client_reject'
        ]
    ],
    'beh_early_repaym_quant' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'beh_early_repaym_quant'
        ]
    ],
    'beh_time_from_last_apl' => [
        'worker' => workers\BehTimeFromLastApl::class,
        'model' => models\BehTimeFromLastApl::class
    ],
    'city_res' => [
        'worker' => workers\CityRes::class,
        'model' => models\CityRes::class
    ],
    'expns_cred_to_req_amnt' => [
        'worker' => workers\ExpnsCredToReqAmnt::class,
        'model' => models\ExpnsCredToReqAmnt::class,
    ],
    'max_dpd_previous_loan' => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'max_dpd_previous_loan'
        ]
    ],
    "previous_auto_declined" => [
        'worker' => workers\AppWorker::class,
        'model' => models\AppModel::class,
        'modelConfig' => [
            'name' => 'previous_auto_declined'
        ]
    ],
    "previous_max_amount_to_requested_amount" => [
        'worker' => workers\PreviousMaxAmountToRequestedAmount::class,
        'model' => models\PreviousMaxAmountToRequestedAmount::class,
    ]
];
