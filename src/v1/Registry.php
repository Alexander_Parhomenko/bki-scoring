<?php

namespace ifinance\scoring\v1;

/**
 * Class Registry
 * @package ifinance\scoring\v1
 */
final class Registry
{
    /**
     * @var array
     */
    private static $data = [];

    /**
     * @var bool
     */
    private static $on = false;

    /**
     * @param string $key
     * @param $value
     */
    public static function add(string $key, $value): void
    {
        if (self::$on) {
            self::$data[$key] = $value;
        }
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public static function get(string $key)
    {
        return self::$data[$key] ?? null;
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function isExist(string $key): bool
    {
        return isset(self::$data[$key]);
    }

    /**
     *
     */
    public static function clean(): void
    {
        self::$data = [];
    }

    /**
     * @param string $key
     */
    public static function remote(string $key): void
    {
        if (self::isExist($key)) {
            unset(self::$data[$key]);
        }
    }

    /**
     *
     */
    public static function on(): void
    {
        self::$on = true;
    }

    /**
     *
     */
    public static function off(): void
    {
        self::$on = false;
    }

    /**
     * Registry constructor.
     */
    private function __construct()
    {
    }
}
