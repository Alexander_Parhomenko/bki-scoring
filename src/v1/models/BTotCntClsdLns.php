<?php

namespace ifinance\scoring\v1\models;

use ifinance\scoring\v1\workers\traits\ArrayStringToLower;

/**
 * Class BTotCntClsdLns
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dlflstat
 */
class BTotCntClsdLns extends BaseModel
{
    use ArrayStringToLower;

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_tot_cnt_clsd_lns';
    }

    /**
     * @return array
     */
    public function getDlflstat(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['dlflstat']);
    }
}
