<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BehTimeFromLastApl
 * @package ifinance\scoring\v1\models
 *
 * @property int $timeLastCredit
 * @property int $orderCreated
 */
class BehTimeFromLastApl extends BaseModel
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'beh_time_from_last_apl';
    }

    public function getTimeLastCredit(): int
    {
        return $this->data['attribute']['app_time_last_credit'];
    }

    /**
     * @return int
     */
    public function getOrderCreated(): int
    {
        return $this->data['attribute']['order_created'];
    }
}
