<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BTotAvgExstAmt
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dlFlStat
 */
class BTotAvgExstAmt extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_tot_avg_exst_amt';
    }

    /**
     * @return string[]
     */
    public function getDlFlStat(): array
    {
        return $this->data['attribute']['dlflstat'];
    }
}
