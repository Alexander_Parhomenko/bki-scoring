<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BTotAvgTotAmt
 * @package ifinance\scoring\v1\models
 */
class BTotAvgTotAmt extends BaseModel
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_tot_avg_tot_amt';
    }
}
