<?php

namespace ifinance\scoring\v1\models;

/**
 * Class ScoreArtellence
 * @package ifinance\scoring\v1\models
 *
 * @property string $score
 */
class ScoreArtellence extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'score_artellence';
    }

    public function getScore(): string
    {
        return $this->data['attribute']['score_artellence'];
    }
}
