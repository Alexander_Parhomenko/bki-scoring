<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BTotMaxOvrdClsdLns
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dlFlStat
 */
class BTotMaxOvrdClsdLns extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_tot_max_ovrd_clsd_lns';
    }

    /**
     * @return string[]
     */
    public function getDlFlStat(): array
    {
        return $this->data['attribute']['dlflstat'];
    }
}
