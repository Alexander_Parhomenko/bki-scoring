<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BTotSumClsdAmt
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dlFlStat
 */
class BTotSumClsdAmt extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_tot_sum_clsd_amt';
    }

    /**
     * @return string[]
     */
    public function getDlFlStat(): array
    {
        return $this->data['attribute']['dlflstat'];
    }
}
