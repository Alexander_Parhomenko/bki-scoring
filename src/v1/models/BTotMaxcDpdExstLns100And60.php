<?php

namespace ifinance\scoring\v1\models;

use ifinance\scoring\v1\workers\traits\ArrayStringToLower;

/**
 * Class BTotMaxcDpdExstLns100And60
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dldonor
 * @property string[] $dlflstat
 * @property float $dldateclc
 * @property float $dlamtexp
 * @property float $created
 */
class BTotMaxcDpdExstLns100And60 extends BaseModel
{
    use ArrayStringToLower;

    /**
     * @return array
     */
    public function getDldonor(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['dldonor']);
    }

    /**
     * @return array
     */
    public function getDlflstat(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['dlflstat']);
    }

    /**
     * @return float
     */
    public function getDldateclc(): float
    {
        return $this->data['attribute']['dldateclc'];
    }

    /**
     * @return float
     */
    public function getDlamtexp(): float
    {
        return $this->data['attribute']['dlamtexp'];
    }

    /**
     * @return float
     */
    public function getCreated(): float
    {
        return $this->data['attribute']['order_created'];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_mfo_maxc_dpd_exst_lns_100_60';
    }
}
