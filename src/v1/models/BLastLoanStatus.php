<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BLastLoanStatus
 * @package ifinance\scoring\v1\models
 */
class BLastLoanStatus extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_last_loan_status';
    }
}
