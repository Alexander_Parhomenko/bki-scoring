<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BTotCntCur7DpdExstLns
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dlFlStat
 * @property int $minDlDayExp
 */
class BTotCntCur7DpdExstLns extends BaseModel
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_tot_cnt_cur7dpd_exst_lns';
    }

    /**
     * @return string[]
     */
    public function getDlFlStat(): array
    {
        return $this->data['attribute']['dlflstat'];
    }

    /**
     * @return int
     */
    public function getMinDlDayExp(): int
    {
        return $this->data['attribute']['min_dldayexp'];
    }
}
