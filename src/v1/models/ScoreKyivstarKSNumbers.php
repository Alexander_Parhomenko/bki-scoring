<?php

namespace ifinance\scoring\v1\models;

/**
 * Class ScoreKyivstarKSNumbers
 * @package ifinance\scoring\v1\models
 *
 * @property string phoneNumber
 * @property string score
 */
class ScoreKyivstarKSNumbers extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'score_kyivstar_ks_numbers';
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->data['attribute']['phone'];
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->data['attribute']['ks_scoring'];
    }
}
