<?php

namespace ifinance\scoring\v1\models;

/**
 * Class ScoreActual
 * @package ifinance\scoring\v1\models
 */
class ScoreActual extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'score_actual';
    }
}
