<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BAvgTermFactMfo
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dlFlStat
 * @property string[] $donor
 */
class BAvgTermFactMfo extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_avg_term_fact_mfo';
    }

    /**
     * @return string[]
     */
    public function getDlFlStat(): array
    {
        return $this->data['attribute']['dlflstat'];
    }

    public function getDonor(): array
    {
        return $this->data['attribute']['donor'];
    }
}
