<?php

namespace ifinance\scoring\v1\models;

/**
 * Class QMfoWk
 * @package ifinance\scoring\v1\models
 */
class QMfoWk extends QMfo
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'q_mfo_wk';
    }
}
