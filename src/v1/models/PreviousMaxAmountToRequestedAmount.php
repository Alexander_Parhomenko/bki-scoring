<?php

namespace ifinance\scoring\v1\models;

/**
 * Class PreviousMaxAmountToRequestedAmount
 * @package ifinance\scoring\v1\models
 *
 * @property float $requestCreditSum
 * @property float $maxCreditSumApprove
 */
class PreviousMaxAmountToRequestedAmount extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'previous_max_amount_to_requested_amount';
    }

    /**
     * @return float
     */
    public function getRequestCreditSum(): float
    {
        return $this->data['attribute']['request_credit_sum'];
    }

    /**
     * @return float
     */
    public function getMaxCreditSumApprove(): float
    {
        return $this->data['attribute']['max_credit_sum_approve'];
    }
}
