<?php

namespace ifinance\scoring\v1\models;

/**
 * Class QMfoQw
 * @package ifinance\scoring\v1\models
 */
class QMfoQw extends QMfo
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'q_mfo_qw';
    }
}
