<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BkiAplEmailMatchBkiAplPhoneMatch
 * @package ifinance\scoring\v1\models
 *
 * @property int[] $emailCType
 * @property int[] $phoneCType
 * @property string $phone
 * @property string $email
 */
class BkiAplEmailMatchBkiAplPhoneMatch extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'bki_apl_email_match_bki_apl_phone_match';
    }

    public function getEmailCType(): array
    {
        return $this->data['attribute']['email_c_type'];
    }

    public function getPhoneCType(): array
    {
        return $this->data['attribute']['phone_c_type'];
    }

    public function getPhone(): string
    {
        return $this->data['attribute']['phone'];
    }

    public function getEmail(): string
    {
        return $this->data['attribute']['email'];
    }
}
