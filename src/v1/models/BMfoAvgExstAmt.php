<?php

namespace ifinance\scoring\v1\models;

use ifinance\scoring\v1\workers\traits\ArrayStringToLower;

/**
 * Class BMfoAvgExstAmt
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dldonor
 * @property string[] $dlflstat
 */
class BMfoAvgExstAmt extends BaseModel
{
    use ArrayStringToLower;

    /**
     * @return string[]
     */
    public function getDldonor(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['dldonor']);
    }

    /**
     * @return array
     */
    public function getDlflstat(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['dlflstat']);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_mfo_avg_exst_amt';
    }
}
