<?php

namespace ifinance\scoring\v1\models;

use ifinance\scoring\v1\workers\traits\ArrayStringToLower;

/**
 * Class BTotSumcExstBalCmr
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dlcelcred
 * @property string[] $dlflstat
 */
class BTotSumcExstBalCmr extends BaseModel
{
    use ArrayStringToLower;

    /**
     * @return array
     */
    public function getDlcelcred(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['dlcelcred']);
    }

    /**
     * @return array
     */
    public function getDlflstat(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['dlflstat']);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_tot_sumc_exst_bal_cmr';
    }
}
