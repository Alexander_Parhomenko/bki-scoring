<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BTotMaxDpdTotLns100
 * @package ifinance\scoring\v1\models
 *
 * @property float $dlamtexp
 */
class BTotMaxDpdTotLns100 extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_tot_max_dpd_tot_lns_100';
    }

    /**
     * @return int
     */
    public function getDlamtexp(): int
    {
        return $this->data['attribute']['dlamtexp'];
    }
}
