<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BkiAplCountOfAdressParamMatch
 * @package ifinance\scoring\v1\models
 *
 * @property string regRegion
 * @property string liveRegion
 * @property string regCity
 * @property string liveCity
 * @property string regStreet
 * @property string liveStreet
 * @property string regBuild
 * @property string liveBuild
 * @property string regFlat
 * @property string liveFlat
 */
class BkiAplCountOfAdressParamMatch extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'bki_apl_count_of_adress_param_match';
    }

    /**
     * @return string
     */
    public function getRegRegion(): ?string
    {
        return $this->data['attribute']['reg']['region'];
    }

    /**
     * @return string
     */
    public function getRegCity(): ?string
    {
        return $this->data['attribute']['reg']['city'];
    }

    /**
     * @return string
     */
    public function getRegStreet(): ?string
    {
        return $this->data['attribute']['reg']['street'];
    }

    /**
     * @return string
     */
    public function getRegBuild(): ?string
    {
        return $this->data['attribute']['reg']['build'];
    }

    /**
     * @return string
     */
    public function getRegFlat(): ?string
    {
        return $this->data['attribute']['reg']['flat'];
    }

    /**
     * @return string
     */
    public function getLiveRegion(): ?string
    {
        return $this->data['attribute']['live']['region'];
    }

    /**
     * @return string
     */
    public function getLiveCity(): ?string
    {
        return $this->data['attribute']['live']['city'];
    }

    /**
     * @return string
     */
    public function getLiveStreet(): ?string
    {
        return $this->data['attribute']['live']['street'];
    }

    /**
     * @return string
     */
    public function getLiveBuild(): ?string
    {
        return $this->data['attribute']['live']['build'];
    }

    /**
     * @return string
     */
    public function getLiveFlat(): ?string
    {
        return $this->data['attribute']['live']['flat'];
    }
}
