<?php

namespace ifinance\scoring\v1\models;

/**
 * Class AppIncNetToReqAmnt
 * @package ifinance\scoring\v1\models
 *
 * @property float $income
 * @property float $expences
 * @property float $otherCreditPayes
 * @property float $orderSum
 */
class AppIncNetToReqAmnt extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'app_inc_net_to_req_amnt';
    }

    /**
     * @return float
     */
    public function getIncome(): float
    {
        return $this->data['attribute']['user_income'];
    }

    /**
     * @return float
     */
    public function getExpences(): float
    {
        return $this->data['attribute']['user_expences'];
    }

    /**
     * @return float
     */
    public function getOtherCreditPayes(): float
    {
        return $this->data['attribute']['user_other_credit_payes'];
    }

    public function getOrderSum(): float
    {
        return $this->data['attribute']['request_credit_sum'];
    }
}
