<?php

namespace ifinance\scoring\v1\models;

/**
 * Class DaysRegistered
 * @package ifinance\scoring\v1\models
 *
 * @property int $created
 * @property int $register
 */
class DaysRegistered extends BaseModel
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'days_registered';
    }

    public function getCreated(): int
    {
        return $this->data['attribute']['order_created'];
    }

    public function getRegister(): int
    {
        return $this->data['attribute']['user_register'];
    }
}
