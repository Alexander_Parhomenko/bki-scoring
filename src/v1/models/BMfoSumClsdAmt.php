<?php

namespace ifinance\scoring\v1\models;

use ifinance\scoring\v1\workers\traits\ArrayStringToLower;

/**
 * Class BMfoSumClsdAmt
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dldonor
 * @property string[] $dlflstat
 */
class BMfoSumClsdAmt extends BaseModel
{
    use ArrayStringToLower;

    /**
     * @return string[]
     */
    public function getDldonor(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['dldonor']);
    }

    /**
     * @return string[]
     */
    public function getDlflstat(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['dlflstat']);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_mfo_sum_clsd_amt';
    }
}
