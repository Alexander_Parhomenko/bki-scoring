<?php

namespace ifinance\scoring\v1\models;

/**
 * Class ExpnsCredToReqAmnt
 * @package ifinance\scoring\v1\models
 *
 * @property float $userOtherCreditPayes
 * @property float $requestCreditSum
 */
class ExpnsCredToReqAmnt extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'expns_cred_to_req_amnt';
    }

    /**
     * @return float
     */
    public function getUserOtherCreditPayes(): float
    {
        return $this->data['attribute']['user_other_credit_payes'];
    }

    /**
     * @return float
     */
    public function getRequestCreditSum(): float
    {
        return $this->data['attribute']['request_credit_sum'];
    }
}
