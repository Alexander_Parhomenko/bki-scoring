<?php

namespace ifinance\scoring\v1\models;

/**
 * Class ScoreVodafone
 * @package ifinance\scoring\v1\models
 *
 * @property string $score
 */
class ScoreVodafone extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'score_vodafone';
    }

    /**
     * @return string
     */
    public function getScore(): string
    {
        return $this->data['attribute']['vodafone_score'];
    }
}
