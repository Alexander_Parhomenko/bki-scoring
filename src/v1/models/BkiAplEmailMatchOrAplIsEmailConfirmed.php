<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BkiAplEmailMatchOrAplIsEmailConfirmed
 * @package ifinance\scoring\v1\models
 *
 * @property string $email
 * @property bool $emailValidate
 * @property string[] $cType
 */
class BkiAplEmailMatchOrAplIsEmailConfirmed extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'bki_apl_email_match_or_apl_is_email_confirmed';
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->data['attribute']['email'];
    }

    /**
     * @return bool
     */
    public function getEmailValidate(): bool
    {
        return $this->data['attribute']['email_validate'];
    }

    /**
     * @return array
     */
    public function getCType(): array
    {
        return $this->data['attribute']['c_type'];
    }

    public function getDiapason(): array
    {
        return [
            [
                'result' => 0,
                'min' => -1,
                'max' => 1,
            ],
        ];
    }
}
