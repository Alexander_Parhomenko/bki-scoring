<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BLastLoanDateClsd
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dlflstat
 * @property int $orderCreated
 */
class BLastLoanDateClsd extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_last_loan_date_clsd';
    }

    public function getDlflstat(): array
    {
        return $this->data['attribute']['dlflstat'];
    }

    /**
     * @return int
     */
    public function getOrderCreated(): int
    {
        return $this->data['attribute']['order_created'];
    }
}
