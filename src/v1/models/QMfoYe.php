<?php

namespace ifinance\scoring\v1\models;

use dstotijn\yii2jsv\JsonSchemaValidator;
use ifinance\scoring\v1\workers\traits\ArrayStringToLower;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Class QMfoYe
 * @package ifinance\scoring\v1\models
 */
class QMfoYe extends QMfo
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'q_mfo_ye';
    }
}
