<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BMfoCntSold12mLns
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $dldonor
 * @property string[] $dlflstat
 * @property int $maxDldateclc
 * @property int $created
 */
class BMfoCntSold12mLns extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'b_mfo_cnt_sold_12m_lns';
    }

    public function getDldonor(): array
    {
        return $this->data['attribute']['dldonor'];
    }

    public function getDlflstat(): array
    {
        return $this->data['attribute']['dlflstat'];
    }

    public function getMaxDldateclc(): int
    {
        return $this->data['attribute']['max_dldateclc'];
    }

    public function getCreated(): int
    {
        return $this->data['attribute']['order_created'];
    }
}
