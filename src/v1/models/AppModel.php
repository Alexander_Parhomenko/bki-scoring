<?php

namespace ifinance\scoring\v1\models;

/**
 * Class AppModel
 * @package ifinance\scoring\v1\models
 *
 * @property mixed $score
 */
class AppModel extends BaseModel
{
    /**
     * @var string
     */
    public $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->data['attribute'][$this->getName()];
    }
}
