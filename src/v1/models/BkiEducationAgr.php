<?php

namespace ifinance\scoring\v1\models;

/**
 * Class BkiEducationAgr
 * @package ifinance\scoring\v1\models
 */
class BkiEducationAgr extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'bki_education_agr';
    }
}
