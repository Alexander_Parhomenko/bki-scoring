<?php

namespace ifinance\scoring\v1\models;

/**
 * Class CityRes
 * @package ifinance\scoring\v1\models
 *
 * @property string|null $city
 * @property array $group
 */
class CityRes extends BaseModel
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'city_res';
    }

    public function getCity(): ?string
    {
        return $this->data['attribute']['live']['city'];
    }

    public function getGroup(): array
    {
        return $this->data['attribute']['group'];
    }
}
