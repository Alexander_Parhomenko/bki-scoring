<?php

namespace ifinance\scoring\v1\models;

use dstotijn\yii2jsv\JsonSchemaValidator;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Class BaseModel
 * @package ifinance\scoring\v1\models
 * @property array diapason
 */
abstract class BaseModel extends Model
{
    /**
     * @var string
     */
    public $json;

    /**
     * @var array
     */
    public $bkiFile = [];

    /**
     * @var array
     */
    public $zone = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $depends = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'json',
                JsonSchemaValidator::class,
                'schema' => 'file://' . __DIR__ . '/schema/' . $this->getName() . '.json'
            ],
            [['zone'], 'required'],
        ];
    }

    /**
     *
     */
    public function afterValidate()
    {
        $this->data = Json::decode($this->json)[$this->getName()];
        parent::afterValidate();
    }

    /**
     * @return string
     */
    abstract public function getName(): string;

    /**
     * @return array
     */
    public function getDiapason(): array
    {
        return $this->data['diapason'];
    }

    /**
     * @param string $name
     * @param float|null $value
     */
    public function setDepends(string $name, ?float $value): void
    {
        $this->depends[$name] = $value;
    }
}
