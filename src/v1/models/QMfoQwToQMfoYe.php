<?php

namespace ifinance\scoring\v1\models;

/**
 * Class QMfoQwToQMfoYe
 * @package ifinance\scoring\v1\models
 *
 * @property float $qMfoQw
 * @property float $qMfoYe
 */
class QMfoQwToQMfoYe extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'q_mfo_qw_to_q_mfo_ye';
    }

    /**
     * @return float
     */
    public function getQMfoQw(): float
    {
        return $this->depends['q_mfo_qw'];
    }

    /**
     * @return float
     */
    public function getQMfoYe(): float
    {
        return $this->depends['q_mfo_ye'];
    }
}
