<?php

namespace ifinance\scoring\v1\models;

/**
 * Class ScoreKyivstarNonKSNumbers
 * @package ifinance\scoring\v1\models
 *
 * @property string phoneNumber
 * @property string score
 */
class ScoreKyivstarNonKSNumbers extends BaseModel
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'score_kyivstar_non_ks_numbers';
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->data['attribute']['phone'];
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->data['attribute']['ks_scoring'];
    }
}
