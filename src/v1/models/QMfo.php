<?php

namespace ifinance\scoring\v1\models;

use ifinance\scoring\v1\workers\traits\ArrayStringToLower;

/**
 * Class QMfo
 * @package ifinance\scoring\v1\models
 *
 * @property string[] $org
 * @property string[] $reqReason
 * @property int $maxReDate
 * @property int $orderCreated
 */
abstract class QMfo extends BaseModel
{
    use ArrayStringToLower;

    /**
     * @return array
     */
    public function getOrg(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['org']);
    }

    /**
     * @return array
     */
    public function getReqReason(): array
    {
        return $this->arrayStringToLower($this->data['attribute']['reqreason']);
    }

    /**
     * @return int
     */
    public function getMaxReDate(): int
    {
        return $this->data['attribute']['max_redate'];
    }

    /**
     * @return int
     */
    public function getOrderCreated(): int
    {
        return $this->data['attribute']['order_created'];
    }
}
