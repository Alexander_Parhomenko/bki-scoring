<?php

namespace ifinance\scoring;

use ifinance\scoring\exceptions\NotFoundVersionException;
use ifinance\scoring\exceptions\NotValidVersionException;
use ifinance\scoring\interfaces\ARunner;
use ifinance\scoring\v1\Runner;

/**
 * Class Version
 * @package ifinance\scoring
 *
 * @method static ARunner current
 * @method static ARunner v1
 * @method static ARunner v2
 */
class Version
{
    const CURRENT = 'current';
    const V1 = 'v1';

    /**
     * @var ARunner[]
     */
    private static $versions = [
        self::CURRENT => Runner::class,
        self::V1 => Runner::class,
    ];

    /**
     * @param $name
     * @param $arguments
     * @return ARunner
     * @throws NotFoundVersionException
     * @throws NotValidVersionException
     */
    public static function __callStatic($name, $arguments): ARunner
    {
        if (!isset(self::$versions[$name])) {
            throw new NotFoundVersionException($name);
        }

        $version = new self::$versions[$name]();

        if ($version instanceof ARunner) {
            return $version;
        } else {
            throw new NotValidVersionException();
        }
    }
}
