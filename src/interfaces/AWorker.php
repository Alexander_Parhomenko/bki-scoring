<?php

namespace ifinance\scoring\interfaces;

use ifinance\scoring\exceptions\NotFountAttributeException;
use ifinance\scoring\v1\Checker;
use yii\base\BaseObject;
use yii\base\Model;

/**
 * Class AWorker
 * @package ifinance\scoring\interfaces
 */
abstract class AWorker extends BaseObject
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @var null|float
     */
    protected $score = null;

    /**
     *
     */
    public function initDefaultScore(): void
    {
        $this->score = null;
    }

    /**
     * @return float
     */
    public function run(): float
    {
        try {
            $this->initDefaultScore();
            $this->initScore();
        } catch (\Throwable $e) {
            Logger::logError($e, Logger::LOG_LEVEL_CRITICAL);
        }
        Logger::log([
            'class' => static::class,
            'score' => $this->score
        ], Logger::LOG_LEVEL_CRITICAL);
        foreach ($this->model->diapason as $item) {
            $result = $item['result'];
            unset($item['result']);
            $checker = new Checker($item);
            if ($checker->check($this->score)) {
                Logger::log([
                    'class' => static::class,
                    'diapason' => $item,
                    'score' => $this->score,
                    'result' => $result,
                    'check' => true
                ], Logger::LOG_LEVEL_ALL);
                return $result;
            }
            Logger::log([
                'class' => static::class,
                'diapason' => $item,
                'score' => $this->score,
                'result' => $result,
                'check' => false
            ], Logger::LOG_LEVEL_ALL);
        }
        return 0;
    }

    /**
     * @return float|null
     */
    public function getScore(): ?float
    {
        return $this->score;
    }

    /**
     *
     */
    abstract protected function initScore(): void;
}
