<?php

namespace ifinance\scoring\interfaces;

use yii\base\BaseObject;

/**
 * Class Response
 * @package ifinance\scoring\interfaces
 *
 * @property string $zone
 * @property float $score
 */
class Response
{
    /**
     * @var array
     */
    private $params = [];

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->params[$name] ?? null;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->params[$name] = $value;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->params;
    }
}
