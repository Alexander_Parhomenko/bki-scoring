<?php

namespace ifinance\scoring\interfaces;

use yii\helpers\Json;

/**
 * Class Logger
 * @package ifinance\scoring\interfaces
 */
final class Logger
{
    const LOG_LEVEL_NONE = -1;
    const LOG_LEVEL_CRITICAL = 1;
    const LOG_LEVEL_INFO = 2;
    const LOG_LEVEL_ALL = 3;

    private static $logLevel = 3;

    /**
     * @var array
     */
    private static $log = [];

    /**
     * @param $data
     * @param int $level
     */
    public static function log($data, int $level = 0): void
    {
        if ($level > self::$logLevel) {
            return;
        }

        $name = debug_backtrace();
        $name = $name[1]['class'];
        $data = Json::encode($data);
        self::$log[$name][] = $data;
    }

    /**
     * @param \Throwable $data
     * @param int $level
     */
    public static function logError(\Throwable $data, int $level = 0): void
    {
        if ($level > self::$logLevel) {
            return;
        }

        $name = debug_backtrace();
        $name = $name[1]['class'];
        $data = Json::encode([
            'file' => $data->getFile(),
            'line' => $data->getLine(),
            'message' => $data->getMessage(),
            'trace' => $data->getTraceAsString(),
        ]);
        self::$log[$name]['error'][] = $data;
    }

    /**
     * @return array
     */
    public static function get(): array
    {
        return self::$log;
    }

    /**
     * @param int $level
     */
    public static function setLogLevel(int $level = self::LOG_LEVEL_ALL): void
    {
        self::$logLevel = $level;
    }

    /**
     *
     */
    public static function clean(): void
    {
        self::$log = [];
    }
}
