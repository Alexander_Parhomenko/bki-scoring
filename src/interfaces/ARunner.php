<?php

namespace ifinance\scoring\interfaces;

use yii\base\BaseObject;

/**
 * Class ARunner
 * @package ifinance\scoring\interfaces
 */
abstract class ARunner extends BaseObject
{
    /**
     * @param string $configure
     * @return Response
     */
    abstract public function run(string $configure): Response;
}
