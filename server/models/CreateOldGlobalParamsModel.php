<?php

namespace server\models;

use yii\base\Model;

/**
 * Class CreateOldGlobalParamsModel
 * @package server\models
 */
class CreateOldGlobalParamsModel extends Model
{
    // int
    public $app_employment_type;
    public $app_industry_type_agr;
    public $app_family_status;
    public $app_position;
    public $app_requested_period;
    public $beh_client_reject;
    public $app_time_last_credit;
    public $max_dpd_previous_loan;
    public $previous_auto_declined;
    public $order_created;
    public $beh_early_repaym_quant;

    // float
    public $user_other_credit_payes;
    public $request_credit_sum;
    public $max_credit_sum_approve;

    // string
    public $email;
    public $phone;
    public $score_artellence;
    public $ks_scoring;
    public $vodafone_score;

    // array
    public $reg;
    public $live;

    // not save
    public $appTimeLastCredit;
    public $orderCreated;
    public $regRegion;
    public $regCity;
    public $regStreet;
    public $regBuild;
    public $regFlat;
    public $liveRegion;
    public $liveCity;
    public $liveStreet;
    public $liveBuild;
    public $liveFlat;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [[
                'regRegion',
                'regCity',
                'regStreet',
                'regBuild',
                'regFlat',
                'liveRegion',
                'liveCity',
                'liveStreet',
                'liveBuild',
                'liveFlat',
                'appTimeLastCredit',
                'orderCreated',

                'app_employment_type',
                'app_industry_type_agr',
                'app_family_status',
                'app_position',
                'app_requested_period',
                'beh_client_reject',
                'app_time_last_credit',
                'max_dpd_previous_loan',
                'previous_auto_declined',
                'order_created',
                'beh_early_repaym_quant',
                'user_other_credit_payes',
                'request_credit_sum',
                'max_credit_sum_approve',
                'reg',
                'live',
                'score_artellence',
                'ks_scoring',
                'vodafone_score',
                'email',
                'phone',
            ], 'required'],
            [[
                'app_employment_type',
                'app_industry_type_agr',
                'app_family_status',
                'app_position',
                'app_requested_period',
                'beh_client_reject',
                'app_time_last_credit',
                'max_dpd_previous_loan',
                'previous_auto_declined',
                'order_created',
                'beh_early_repaym_quant',
            ], 'integer'],
            [[
                'user_other_credit_payes',
                'request_credit_sum',
                'max_credit_sum_approve',
            ], 'number'],
            [[
                'score_artellence',
                'ks_scoring',
                'vodafone_score',
                'email',
                'phone',
            ], 'string']
        ];
    }

    /**
     * @return bool|void
     */
    public function beforeValidate()
    {
        $this->reg = [
            'region' => $this->regRegion,
            'city' => $this->regCity,
            'street' => $this->regStreet,
            'build' => $this->regBuild,
            'flat' => $this->regFlat
        ];

        $this->live = [
            'region' => $this->liveRegion,
            'city' => $this->liveCity,
            'street' => $this->liveStreet,
            'build' => $this->liveBuild,
            'flat' => $this->liveFlat
        ];

        foreach ([' ', '-', '(', ')'] as $s) {
            $this->phone = str_replace($s, '', $this->phone);
        }

        $this->order_created = strtotime($this->orderCreated);
        $this->app_time_last_credit = strtotime($this->appTimeLastCredit);

        $this->convertToInt([
            'app_employment_type',
            'app_industry_type_agr',
            'app_family_status',
            'app_position',
            'app_requested_period',
            'beh_client_reject',
            'app_time_last_credit',
            'max_dpd_previous_loan',
            'previous_auto_declined',
            'order_created',
            'beh_early_repaym_quant',
        ]);

        $this->convertToFloat([
            'user_other_credit_payes',
            'request_credit_sum',
            'max_credit_sum_approve',
        ]);

        return parent::beforeValidate();
    }

    /**
     * @param string[] $param
     */
    private function convertToInt(array $param)
    {
        foreach ($param as $name) {
            $this->{$name} = (int)$this->{$name};
        }
    }

    /**
     * @param array $param
     */
    private function convertToFloat(array $param)
    {
        foreach ($param as $name) {
            $this->{$name} = (float)$this->{$name};
        }
    }

    public static function getListFamilyStatus(): array
    {
        return [
            0 => 'FAMILY_MARRIED',
            1 => 'FAMILY_NOT_MARRIED',
            2 => 'FAMILY_DIVORCED',
            3 => 'FAMILY_CIVIL',
        ];
    }

    public static function getListEmploymentType(): array
    {
        return [
            0 => 'работаю официально',
            1 => 'работаю не официально',
            2 => 'собственное дело',
            3 => 'не работаю',
        ];
    }

    public static function getListPositionType(): array
    {
        return [
            0 => 'рабочий',
            1 => 'специалист',
            2 => 'руководитель',
        ];
    }

    public static function getIndustryTypeAgr(): array
    {
        return [
            0 => 'Вооруженные силы Украины',
            1 => 'Госслужба',
            2 => 'IT',
            3 => 'культура и исскуство',
            4 => 'медицина',
            5 => 'наука и образование',
            6 => 'промышленость',
            7 => 'сервисы и услуги',
            8 => 'строительство',
            9 => 'транспорт и перевозки',
            10 => 'финансы',
            11 => 'сетевой маркетинг',
            12 => 'торговля',
            25 => 'другое'
        ];
    }
}
