<?php

namespace server\models;

use yii\base\Model;
use yii\helpers\Json;

/**
 * Class CreateParamsModel
 * @package server\models
 */
abstract class CreateParamsModel extends Model
{
    /**
     * @var bool
     */
    public $all = false;

    /**
     * @var string
     */
    protected $type = '';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [$this->getAllParams(), 'boolean']
        ];
    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        if ($this->all) {
            foreach ($this->getAllParams() as $name) {
                $this->{$name} = true;
            }
            $this->all = false;
        }
        $res = [];
        foreach ($this->getAllParams() as $param) {
            if ($this->{$param}) {
                $data = Json::decode(file_get_contents(__DIR__ . '/mock/' . $this->type . '/' . $param . '.json'));
                foreach ($data as $key => $value) {
                    $res[$key] = $value;
                }
            }
        }
        return Json::encode($res);
    }

    /**
     * @return array
     */
    abstract public function getAllParams(): array;
}
