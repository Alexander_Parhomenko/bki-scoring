<?php

namespace server\models;

use yii\base\Model;
use yii\helpers\Json;

/**
 * Class TestModel
 * @package server\models
 */
class TestModel extends Model
{
    public $bkiFile;
    public $params;
    public $global;
    public $zone = '{
    "Zone 1": {
      "min": 879,
      "max": 1000
    },
    "Zone 2": {
      "min": 763,
      "max": 879
    },
    "Zone 3": {
      "min": 698,
      "max": 763
    },
    "Zone 4": {
      "min": 612,
      "max": 698
    },
    "Zone 5": {
      "min": 504,
      "max": 612
    },
    "Zone 6": {
      "min": 356,
      "max": 504
    },
    "Zone 7": {
      "min": 0,
      "max": 356
    }
  }';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['bkiFile', 'zone', 'params', 'global'], 'required'],
            [['bkiFile', 'zone', 'params', 'global'], 'string'],
        ];
    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        return Json::encode([
            'bki_file' => Json::decode($this->bkiFile),
            'zone' => Json::decode($this->zone),
            'params' => Json::decode($this->params),
            'global_data' => Json::decode($this->global),
        ]);
    }
}
