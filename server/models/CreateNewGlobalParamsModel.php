<?php

namespace server\models;

use yii\base\Model;

/**
 * Class CreateNewGlobalParamsModel
 * @package server\models
 */
class CreateNewGlobalParamsModel extends Model
{
    public $orderCreated;
    public $order_created;
    public $app_education_type;
    public $app_fb_connect;
    public $user_income;
    public $user_expences;
    public $user_other_credit_payes;
    public $request_credit_sum;
    public $app_industry_type_agr;
    public $app_requested_period;
    public $app_type_residance;
    public $email;
    public $phone;
    public $email_validate;
    public $user_register;
    public $userRegister;
    public $score_artellence;
    public $ks_scoring;
    public $vodafone_score;

    public $reg;
    public $regRegion;
    public $regCity;
    public $regStreet;
    public $regBuild;
    public $regFlat;

    public $live;
    public $liveRegion;
    public $liveCity;
    public $liveStreet;
    public $liveBuild;
    public $liveFlat;

    public function rules()
    {
        return [
            [[
                'order_created',
                'orderCreated',
                'app_education_type',
                'app_fb_connect',
                'user_income',
                'user_expences',
                'user_other_credit_payes',
                'request_credit_sum',
                'app_industry_type_agr',
                'app_requested_period',
                'app_type_residance',
                'email',
                'phone',
                'email_validate',
                'user_register',
                'userRegister',
                'score_artellence',
                'ks_scoring',
                'vodafone_score',
                'regRegion',
                'regCity',
                'regStreet',
                'regBuild',
                'regFlat',
                'liveRegion',
                'liveCity',
                'liveStreet',
                'liveBuild',
                'liveFlat',
            ], 'required'],
            [['app_fb_connect', 'email_validate'], 'boolean'],
            [['user_income', 'user_expences', 'user_other_credit_payes', 'request_credit_sum'], 'number'],
            ['order_created', 'integer'],
            ['email', 'email'],
            [[
                'regRegion',
                'regCity',
                'regStreet',
                'regBuild',
                'regFlat',
                'liveRegion',
                'liveCity',
                'liveStreet',
                'liveBuild',
                'liveFlat',
            ], 'string']
        ];
    }

    public function beforeValidate()
    {
        $this->order_created = strtotime($this->orderCreated);
        $this->user_register = strtotime($this->userRegister);
        $this->convertToInt([
            'app_education_type',
            'app_industry_type_agr',
            'app_requested_period',
            'app_fb_connect',
            'app_type_residance',
        ]);
        $this->convertToFloat([
            'user_income',
            'user_expences',
            'user_other_credit_payes',
            'request_credit_sum',
        ]);
        $this->email_validate = (bool)$this->email_validate;
        $this->reg = [
            'region' => $this->regRegion,
            'city' => $this->regCity,
            'street' => $this->regStreet,
            'build' => $this->regBuild,
            'flat' => $this->regFlat
        ];

        $this->live = [
            'region' => $this->liveRegion,
            'city' => $this->liveCity,
            'street' => $this->liveStreet,
            'build' => $this->liveBuild,
            'flat' => $this->liveFlat
        ];
        foreach ([' ', '-', '(', ')'] as $s) {
            $this->phone = str_replace($s, '', $this->phone);
        }
        return parent::beforeValidate();
    }

    /**
     * @param string[] $param
     */
    private function convertToInt(array $param)
    {
        foreach ($param as $name) {
            $this->{$name} = (int)$this->{$name};
        }
    }

    private function convertToFloat(array $param)
    {
        foreach ($param as $name) {
            $this->{$name} = (float)$this->{$name};
        }
    }

    public static function getEducationType(): array
    {
        return [
            0 => 'EDUCATION_NOT_FULL_MIDDLE',
            1 => 'EDUCATION_MIDDLE',
            2 => 'EDUCATION_MIDDLE_SPECIAL',
            3 => 'EDUCATION_NOT_FULL_HIGH',
            4 => 'EDUCATION_HIGH',
            5 => 'EDUCATION_TWO_HEIGHT',
            6 => 'EDUCATION_SCIENCE_DEGREE'
        ];
    }

    public static function getIndustryTypeAgr(): array
    {
        return [
            0 => 'Вооруженные силы Украины',
            1 => 'Госслужба',
            2 => 'IT',
            3 => 'культура и исскуство',
            4 => 'медицина',
            5 => 'наука и образование',
            6 => 'промышленость',
            7 => 'сервисы и услуги',
            8 => 'строительство',
            9 => 'транспорт и перевозки',
            10 => 'финансы',
            11 => 'сетевой маркетинг',
            12 => 'торговля',
            25 => 'другое'
        ];
    }

    public static function getTypeResidance(): array
    {
        return [
            0 => 'LIVING_TYPE_OWN_APARTMENT',
            1 => 'LIVING_TYPE_RELATIVES',
            2 => 'LIVING_TYPE_DORMITORY',
            3 => 'LIVING_TYPE_RENT'
        ];
    }
}
