<?php

namespace server\models;

/**
 * Class CreateNewParamsModel
 * @package server\models
 */
class CreateNewParamsModel extends CreateParamsModel
{
    protected $type = 'new';

    public $qMfoYe = false;
    public $bMfoAvgExstAmt = false;
    public $bMfoSumClsdAmt = false;
    public $bMfoMaxcDpdExstLns100And60 = false;
    public $bTotSumcExstBalCmr = false;
    public $bkiAplEmailMatchBkiAplPhoneMatch = false;
    public $bkiEducationAgr = false;
    public $scoreActual = false;
    public $scoreVodafone = false;
    public $scoreKyivstarKSNumbers = false;
    public $scoreKyivstarNonKSNumbers = false;
    public $scoreArtellence = false;
    public $appEducationType = false;
    public $appIndustryTypeAgr = false;
    public $appRequestedPeriod = false;
    public $daysRegistered = false;
    public $appFb = false;
    public $appIncNetToReqAmnt = false;
    public $appTypeResidance = false;
    public $bTotCntClsdLns = false;
    public $bTotAvgTotAmt = false;
    public $bTotMaxDpdTotLns100 = false;
    public $bLastLoanDateClsd = false;
    public $bLastLoanStatus = false;
    public $bMfoCntSold12mLns = false;
    public $bkiAplEmailMatchOrAplIsEmailConfirmed = false;

    /**
     * @return array
     */
    public function getAllParams(): array
    {
        return [
            'qMfoYe',
            'bMfoAvgExstAmt',
            'bMfoSumClsdAmt',
            'bMfoMaxcDpdExstLns100And60',
            'bTotSumcExstBalCmr',
            'bkiAplEmailMatchBkiAplPhoneMatch',
            'bkiEducationAgr',
            'scoreActual',
            'scoreVodafone',
            'scoreKyivstarKSNumbers',
            'scoreKyivstarNonKSNumbers',
            'scoreArtellence',
            'appEducationType',
            'appIndustryTypeAgr',
            'appRequestedPeriod',
            'daysRegistered',
            'appFb',
            'appIncNetToReqAmnt',
            'appTypeResidance',
            'bTotCntClsdLns',
            'bTotAvgTotAmt',
            'bTotMaxDpdTotLns100',
            'bLastLoanDateClsd',
            'bLastLoanStatus',
            'bMfoCntSold12mLns',
            'bkiAplEmailMatchOrAplIsEmailConfirmed',
            'all'
        ];
    }
}
