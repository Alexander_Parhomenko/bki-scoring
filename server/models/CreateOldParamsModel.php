<?php

namespace server\models;

/**
 * Class CreateOldParamsModel
 * @package server\models
 */
class CreateOldParamsModel extends CreateParamsModel
{
    protected $type = 'old';

    public $aplFromLastAcpt = false;
    public $appEducationType = false;
    public $appEmploymentType = false;
    public $appFamilyStatus = false;
    public $appIndustryType = false;
    public $appPosition = false;
    public $appRequestedPeriod = false;
    public $bAvgTermFactMfo = false;
    public $behClientReject = false;
    public $behEarlyRepaymQuant = false;
    public $behTimeFromLastApl = false;
    public $bkiAplCountOfAdressParamMatch = false;
    public $bLastLoanDateClsd = false;
    public $bLastLoanStatus = false;
    public $bMfoAvgExstAmt = false;
    public $bTotAvgExstAmt = false;
    public $bTotCntCur7DpdExstLns = false;
    public $bTotMaxDpdTotLns100 = false;
    public $bTotMaxOvrdClsdLns = false;
    public $bTotSumClsdAmt = false;
    public $cityRes = false;
    public $expnsCredToReqAmnt = false;
    public $maxDPDPreviousLoan = false;
    public $oldBkiAplEmailMatchBkiAplPhoneMatch = false;
    public $previousAutoDeclined = false;
    public $previousMaxAmountToRequestedAmount = false;
    public $qMfoQw = false;
    public $qMfoQwToQMfoYe = false;
    public $qMfoWk = false;
    public $qMfoYe = false;
    public $scoreActual = false;
    public $scoreArtellence = false;
    public $scoreKyivstarKSNumbers = false;
    public $scoreVodafone = false;

    /**
     * @return array
     */
    public function getAllParams(): array
    {
        return [
            'aplFromLastAcpt',
            'appEducationType',
            'appEmploymentType',
            'appFamilyStatus',
            'appIndustryType',
            'appPosition',
            'appRequestedPeriod',
            'bAvgTermFactMfo',
            'behClientReject',
            'behEarlyRepaymQuant',
            'behTimeFromLastApl',
            'bkiAplCountOfAdressParamMatch',
            'bLastLoanDateClsd',
            'bLastLoanStatus',
            'bMfoAvgExstAmt',
            'bTotAvgExstAmt',
            'bTotCntCur7DpdExstLns',
            'bTotMaxDpdTotLns100',
            'bTotMaxOvrdClsdLns',
            'bTotSumClsdAmt',
            'cityRes',
            'expnsCredToReqAmnt',
            'maxDPDPreviousLoan',
            'oldBkiAplEmailMatchBkiAplPhoneMatch',
            'previousAutoDeclined',
            'previousMaxAmountToRequestedAmount',
            'qMfoQw',
            'qMfoQwToQMfoYe',
            'qMfoWk',
            'qMfoYe',
            'scoreActual',
            'scoreArtellence',
            'scoreKyivstarKSNumbers',
            'scoreVodafone',
            'all',
        ];
    }
}
