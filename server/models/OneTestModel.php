<?php

namespace server\models;

use yii\base\Model;

/**
 * Class OneTestModel
 * @package server\models
 */
class OneTestModel extends Model
{
    public $configure;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['configure'], 'required'],
            [['configure'], 'string'],
        ];
    }
}
