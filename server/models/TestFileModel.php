<?php

namespace server\models;

use yii\base\Model;

/**
 * Class TestFileModel
 * @package server\models
 */
class TestFileModel extends Model
{
    public $configure;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['configure'], 'required'],
            [['configure'], 'file'],
        ];
    }
}
