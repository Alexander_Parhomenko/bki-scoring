<?php

/* @var $this \yii\web\View */
/* @var $model \server\models\CreateParamsModel */
$fields = $model->getAllParams();

use yii\widgets\Breadcrumbs;

echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n",
    'links' => [
        'Create params',
        'new',
    ],
]);

?>

<?php $form = \yii\widgets\ActiveForm::begin(); ?>
<div class="panel panel-primary">
    <div class="panel-heading">Options</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <?php
                foreach ($fields as $id => $param) {
                    if ($id % 2 === 0) {
                        echo $form->field($model, $param)->widget(
                            \kartik\checkbox\CheckboxX::class,
                            [
                                'pluginOptions' => [
                                    'threeState' => false,
                                    'size' => 'md'
                                ]
                            ]
                        );
                    }
                }
                ?>
            </div>
            <div class="col-md-6">
                <?php
                foreach ($fields as $id => $param) {
                    if ($id % 2 !== 0) {
                        echo $form->field($model, $param)->widget(
                            \kartik\checkbox\CheckboxX::class,
                            [
                                'pluginOptions' => [
                                    'threeState' => false
                                ]
                            ]
                        );
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-6 col-md-6">
                <?= \yii\helpers\Html::submitButton('Generate', ['class' => 'btn btn-success']); ?>
            </div>
        </div>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end(); ?>