<?php
/* @var $this \yii\web\View */
/* @var $content string */

\yii\web\YiiAsset::register($this);
\yii\web\JqueryAsset::register($this);
\yii\bootstrap\BootstrapAsset::register($this);

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Json;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body><?php $this->beginBody() ?>
<?php
NavBar::begin(['brandLabel' => 'Scoring', 'options' => ['class' => 'navbar-inverse']]);
echo Nav::widget([
    'items' => [
        ['label' => 'Test', 'url' => ['/site/index']],
        ['label' => 'Test File', 'url' => ['/site/test-file']],
        ['label' => 'Create params', 'items' => [
            ['label' => 'new', 'url' => ['/create-params/new']],
            ['label' => 'old', 'url' => ['/create-params/old']]
        ]],
        ['label' => 'Create global params', 'items' => [
            ['label' => 'new', 'url' => ['/create-global-params/new']],
            ['label' => 'old', 'url' => ['/create-global-params/old']]
        ]],
    ],
    'options' => ['class' => 'navbar-nav'],
]);
NavBar::end();
?>
<div class="container">
    <?= $content ?>
</div>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Logger</div>
        <div class="panel-body">
            <?php foreach (\ifinance\scoring\interfaces\Logger::get() as $class => $items) : ?>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><?= $class; ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($items as $id => $item) : ?>
                            <?php if (is_array($item)) :
                                foreach ($item as &$val) {
                                    $val = Json::decode($val);
                                }
                                ?>
                                <tr class="warning">
                                    <td><?= $id; ?></td>
                                    <td><?= \kdn\yii2\JsonEditor::widget([
                                            'clientOptions' => ['modes' => ['code', 'tree']],
                                            'name' => rand(),
                                            'value' => Json::encode($item)
                                        ]) ?></td>
                                </tr>
                            <?php else : ?>
                                <tr class="warning">
                                    <td><?= $id; ?></td>
                                    <td><?= $item; ?></td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
