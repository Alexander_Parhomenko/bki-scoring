<?php

use server\models\CreateOldGlobalParamsModel;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\widgets\Breadcrumbs;

/* @var $this View */
/* @var $model CreateOldGlobalParamsModel */

$form = ActiveForm::begin();

echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n",
    'links' => [
        'Create global params',
        'old',
    ],
]);
?>

    <div class="panel panel-primary">
        <div class="panel-heading">Global Options for old</div>
        <div class="panel-body">
            <?php dump($model->getErrors()); ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'orderCreated')->widget(\kartik\date\DatePicker::class); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'appTimeLastCredit')->widget(\kartik\date\DatePicker::class); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'app_family_status')->dropDownList($model::getListFamilyStatus()); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'app_employment_type')->dropDownList($model::getListEmploymentType()); ?>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        <ul>
                            <li>Max Credit Sum Approve - Максимально сумма одобренного кредита</li>
                            <li>User Other Credit Payes - выплаты по другим кредитам</li>
                            <li>Request Credit Sum - запрошенная сумма</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'max_credit_sum_approve')->widget(\kartik\number\NumberControl::class) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'user_other_credit_payes')->widget(\kartik\number\NumberControl::class) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'request_credit_sum')->widget(\kartik\number\NumberControl::class) ?>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        <ul>
                            <li>Beh Client Reject - К-во положительно принятых решений по заявкам</li>
                            <li>Beh Early Repaym Quant - К-во досрочных погашений по пердыдущим выданным займам</li>
                            <li>Max Dpd Previous Loan - Максимальное количетсво дней просрочки
                                по последнему закрытому займу</li>
                            <li>Previous Auto Declined - Количество автоматических отказов по пердыдущим заявкам</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'beh_client_reject')->input('number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'beh_early_repaym_quant')->input('number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'max_dpd_previous_loan')->input('number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'previous_auto_declined')->input('number') ?>
                </div>
                <div class="col-md-12 alert alert-success">&nbsp;</div>
                <div class="col-md-4">
                    <?= $form->field($model, 'app_position')->dropDownList($model::getListPositionType()); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'app_industry_type_agr')->dropDownList($model::getIndustryTypeAgr()); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'email')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                        "mask" => "+380 (99) 999-9999"
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'app_requested_period')->input('number'); ?>
                </div>
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                </div>
                <div class="col-md-12">
                    <div class="alert alert-success">Score</div>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'score_artellence')->widget(\kartik\number\NumberControl::class); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'ks_scoring')->widget(\kartik\number\NumberControl::class); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'vodafone_score')->widget(\kartik\number\NumberControl::class); ?>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-success">Reg Address</div>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regRegion')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regCity')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regStreet')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regBuild')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regFlat')->textInput(); ?>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-success">Live Address</div>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveRegion')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveCity')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveStreet')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveBuild')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveFlat')->textInput(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-6 col-md-6">
                    <?= \yii\helpers\Html::submitButton('Generate', ['class' => 'btn btn-success']); ?>
                </div>
            </div>
        </div>
    </div>

<?php \yii\widgets\ActiveForm::end(); ?>