<?php

use server\models\CreateNewGlobalParamsModel;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\widgets\Breadcrumbs;

/* @var $this View */
/* @var $model CreateNewGlobalParamsModel */

$form = ActiveForm::begin();

function checkbox(ActiveForm $form, CreateNewGlobalParamsModel $model, string $name): string
{
    return $form
        ->field($model, $name)
        ->widget(
            \kartik\checkbox\CheckboxX::class,
            [
                'pluginOptions' => [
                    'threeState' => false
                ]
            ]
        );
}

echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n",
    'links' => [
        'Create global params',
        'old',
    ],
]);
?>

    <div class="panel panel-primary">
        <div class="panel-heading">Global Options</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'orderCreated')->widget(\kartik\date\DatePicker::class); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'userRegister')->widget(\kartik\date\DatePicker::class); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'app_education_type')->dropDownList($model::getEducationType()); ?>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        <ul>
                            <li>User Income - ЗП</li>
                            <li>User Expences - растраты</li>
                            <li>User Other Credit Payes - выплаты по другим кредитам</li>
                            <li>Request Credit Sum - запрошенная сумма</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'user_income')->widget(\kartik\number\NumberControl::class) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'user_expences')->widget(\kartik\number\NumberControl::class) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'user_other_credit_payes')->widget(\kartik\number\NumberControl::class) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'request_credit_sum')->widget(\kartik\number\NumberControl::class) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'app_industry_type_agr')->dropDownList($model::getIndustryTypeAgr()); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'app_type_residance')->dropDownList($model::getTypeResidance()); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'email')->textInput(); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                        "mask" => "+380 (99) 999-9999"
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'app_requested_period')->input('number'); ?>
                </div>
                <div class="col-md-3">
                    <?= checkbox($form, $model, 'app_fb_connect'); ?>
                </div>
                <div class="col-md-3">
                    <?= checkbox($form, $model, 'email_validate'); ?>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-success">Score</div>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'score_artellence')->widget(\kartik\number\NumberControl::class); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'ks_scoring')->widget(\kartik\number\NumberControl::class); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'vodafone_score')->widget(\kartik\number\NumberControl::class); ?>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-success">Reg Address</div>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regRegion')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regCity')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regStreet')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regBuild')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'regFlat')->textInput(); ?>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-success">Live Address</div>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveRegion')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveCity')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveStreet')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveBuild')->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'liveFlat')->textInput(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-6 col-md-6">
                    <?= \yii\helpers\Html::submitButton('Generate', ['class' => 'btn btn-success']); ?>
                </div>
            </div>
        </div>
    </div>

<?php \yii\widgets\ActiveForm::end(); ?>