<?php

use kdn\yii2\JsonEditor;
use Yii;

/* @var $this \yii\web\View */
/* @var $model \server\models\TestModel */
/* @var $result array|null */

$this->registerJsFile('/js/index.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<?php if ($result !== null) : ?>
    <div class="panel panel-info">
        <div class="panel-heading">Result</div>
        <div class="panel-body">
            <?= JsonEditor::widget([
                'clientOptions' => ['mode' => 'view'],
                'collapseAll' => ['view'],
                'name' => 'viewer',
                'value' => \yii\helpers\Json::encode($result),
            ]) ?>
        </div>
    </div>
<?php endif; ?>
<?php
$form = \yii\widgets\ActiveForm::begin([
    'id' => 'test-form',
    'options' => ['enctype' => 'multipart/form-data']
]);
?>
<div class="panel panel-primary">
    <div class="panel-heading">Text input</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'bkiFile')->widget(
                    JsonEditor::class,
                    [
                        'clientOptions' => ['modes' => ['code', 'tree']],
                    ]
                ); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'zone')->widget(
                    JsonEditor::class,
                    [
                        'clientOptions' => ['modes' => ['code', 'tree']],
                    ]
                ); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'params')->widget(
                    JsonEditor::class,
                    [
                        'clientOptions' => ['modes' => ['code', 'tree']],
                    ]
                ); ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'global')->widget(
                    JsonEditor::class,
                    [
                        'clientOptions' => ['modes' => ['code', 'tree']],
                    ]
                ); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-5 col-md-1 col-xs-6">
                <?= \yii\helpers\Html::submitButton('Test', ['class' => 'btn btn-primary']); ?>
            </div>
            <div class="col-md-6 col-xs-6">
                <?= \yii\helpers\Html::a('Save', '#test-form', ['class' => 'btn btn-primary', 'id' => 'save']); ?>
            </div>
        </div>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end(); ?>

<script type="text/javascript">
    var SaveUrl = '<?=\yii\helpers\Url::toRoute(['/site/save-index'])?>';
</script>
