<?php

use kdn\yii2\JsonEditor;

/* @var $this \yii\web\View */
/* @var $model \server\models\OneTestModel */
/* @var $result array|null */
?>
<?php if ($result !== null) : ?>
    <div class="panel panel-info">
        <div class="panel-heading">Result</div>
        <div class="panel-body">
            <?= JsonEditor::widget([
                'clientOptions' => ['mode' => 'view'],
                'collapseAll' => ['view'],
                'name' => 'viewer',
                'value' => \yii\helpers\Json::encode($result),
            ]) ?>
        </div>
    </div>
<?php endif; ?>
<?php
$form = \yii\widgets\ActiveForm::begin([
    'id' => 'test-form',
    'options' => ['enctype' => 'multipart/form-data']
]);
?>
    <div class="panel panel-primary">
        <div class="panel-heading">Text input</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'configure')->widget(
                        JsonEditor::class,
                        [
                            'clientOptions' => ['modes' => ['code', 'tree']],
                        ]
                    ); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lf-offset-1 col-lg-11">
                    <?= \yii\helpers\Html::submitButton('Test', ['class' => 'btn btn-primary']); ?>
                </div>
            </div>
        </div>
    </div>
<?php \yii\widgets\ActiveForm::end(); ?>