<?php

namespace server\controllers;

use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\Version;
use server\models\TestFileModel;
use server\models\TestModel;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;
use Yii;

/**
 * Class SiteController
 * @package micro\controllers
 */
class SiteController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Logger::setLogLevel(100);
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $result = null;
        try {
            $model = new TestModel();
            if (\Yii::$app->request->isPost) {
                $model->load(\Yii::$app->request->post());
                if ($model->validate()) {
                    $runner = Version::current()->run($model->getJson());
                    $result = $runner->getAll();
                }
            }
        } catch (\Throwable $e) {
            $result = [
                'status' => 'error',
                'exception' => [
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTraceAsString()
                ]
            ];
        }
        return $this->render('index', ['model' => $model, 'result' => $result]);
    }

    /**
     * @return string|void
     */
    public function actionSaveIndex()
    {
        if (\Yii::$app->request->isPost) {
            $model = new TestModel();
            $model->load(\Yii::$app->request->post());
            if ($model->validate()) {
                return $model->getJson();
            }
        }
    }

    /**
     * @return string
     */
    public function actionTestFile()
    {
        $result = null;
        try {
            $model = new TestFileModel();
            if (\Yii::$app->request->isPost) {
                $post = \Yii::$app->request->post();
//            $post = Json::decode($post);
                $model->configure = UploadedFile::getInstance($model, 'configure');
                if ($model->validate()) {
                    $name = rand() . '.json';
                    if (!$model->configure->saveAs('tmp/' . $name)) {
                        $result = '{status: "error"}';
                    } else {
                        $data = file_get_contents(__DIR__ . '/../web/tmp/' . $name);
                        unlink(__DIR__ . '/../web/tmp/' . $name);
                        $runner = Version::current()->run($data);
                        $result = $runner->getAll();
                    }
                }
            }
        } catch (\Throwable $e) {
            $result = [
                'status' => 'error',
                'exception' => [
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTraceAsString()
                ]
            ];
        }
        return $this->render('test-file', ['model' => $model, 'result' => $result]);
    }
}
