<?php

namespace server\controllers;

use server\models\CreateNewParamsModel;
use server\models\CreateOldParamsModel;
use yii\web\Controller;
use Yii;
use yii\web\RangeNotSatisfiableHttpException;

/**
 * Class CreateParamsController
 * @package server\controllers
 */
class CreateParamsController extends Controller
{
    /**
     * @return string|void
     * @throws RangeNotSatisfiableHttpException
     */
    public function actionNew()
    {
        $model = new CreateNewParamsModel();
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                Yii::$app->response->sendContentAsFile($model->getJson(), 'generate_new.json');
                return;
            }
        }
        return $this->render('new', ['model' => $model]);
    }

    /**
     * @return string|void
     * @throws RangeNotSatisfiableHttpException
     */
    public function actionOld()
    {
        $model = new CreateOldParamsModel();
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                Yii::$app->response->sendContentAsFile($model->getJson(), 'generate_old.json');
                return;
            }
        }
        return $this->render('old', ['model' => $model]);
    }
}
