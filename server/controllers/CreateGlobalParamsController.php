<?php

namespace server\controllers;

use server\models\CreateNewGlobalParamsModel;
use server\models\CreateOldGlobalParamsModel;
use yii\helpers\Json;
use yii\web\Controller;
use Yii;

/**
 * Class CreateGlobalParamsController
 * @package server\controllers
 */
class CreateGlobalParamsController extends Controller
{
    /**
     * @return string|void
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionNew()
    {
        $model = new CreateNewGlobalParamsModel();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                Yii::$app->response->sendContentAsFile(Json::encode($model->attributes), 'global_new.json');
                return;
            }
        }

        return $this->render('new', ['model' => $model]);
    }

    /**
     * @return string|void
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionOld()
    {
        $model = new CreateOldGlobalParamsModel();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                Yii::$app->response->sendContentAsFile(Json::encode($model->attributes), 'global_old.json');
                return;
            }
        }

        return $this->render('old', ['model' => $model]);
    }
}
