<?php

return [
    'id' => 'server-scoring',
    'basePath' => __DIR__,
    'controllerNamespace' => 'server\controllers',
    'components' => [
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'cookieValidationKey' => sha1('test'),
        ],
    ],
    'aliases' => [
        '@server' => __DIR__
    ],
];
