<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$bkiFile = \yii\helpers\Json::decode(file_get_contents(__DIR__ . '/mock/QMfoYe/bki_file.json'));
$params = \yii\helpers\Json::decode(file_get_contents(__DIR__ . '/mock/QMfoYe/QMfoYe.json'));
$zone = [
    'Zone 1' => ["min" => null, "max" => null],
    'Zone 2' => ["min" => 0, "max" => 10],
    'Zone 3' => ["min" => 10],
];

$configure = \yii\helpers\Json::encode([
    'bki_file' => $bkiFile,
    'zone' => $zone,
    'params' => $params,
]);

$run = \ifinance\scoring\Version::current();
dump(
    $run->run($configure),
    \ifinance\scoring\interfaces\Logger::get()
);
