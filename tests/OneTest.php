<?php

namespace tests;

use ifinance\scoring\interfaces\Logger;
use ifinance\scoring\Version;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Finder\Finder;
use yii\helpers\Json;
use Generator;

/**
 * Class OneTest
 * @package tests
 */
class OneTest extends TestCase
{
    /**
     * @param string $config
     * @param $expected
     * @dataProvider dataProviderTestAll
     */
    public function testAll(string $config, array $expected)
    {
        $v = Version::current()->run($config);
        $this->assertEquals($v->getAll(), $expected);
    }

//    public function testGenerate()
//    {
//        $finder = new Finder();
//        $finder->files()->in(__DIR__ . '/mock/all')->name('*.json');
//        foreach ($finder as $file) {
//            $res = Version::current()->run(file_get_contents($file->getRealPath()));
//            $json = json_encode($res->getAll());
//            file_put_contents($file->getRealPath() . '.res', $json);
//            $this->assertEquals($res->getAll(), json_decode($json, true));
//            $this->assertEquals(true, true);
//        }
//    }

    /**
     * @return Generator
     */
    public function dataProviderTestAll(): \Generator
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__ . '/mock/all')->name('*.json');

        foreach ($finder as $file) {
            if (file_exists($file->getRealPath() . '.res')) {
                $config = file_get_contents($file->getRealPath());
                $expected = json_decode(file_get_contents($file->getRealPath() . '.res'), true);
                yield [$config, $expected];
            }
        }
    }
}
